#include "vampiresProject.h"
#include<numeric>



/********************************************************************************************************/
FirstInterface::FirstInterface()
/** Constructor of the First Interface (build ui interface).
 * Add all widgets to display
 *  
 * */
{
	
	// Initialize the main window
    this->set_title("--Vampires-- First Interface");
    this->set_border_width(10);
    this->set_position(Gtk::WIN_POS_CENTER);
	this->resize(800, 600); 
	
	
	// INTIALIZATION OF VAMPIRE OBJECT VALUES
	ourVampire.InitialiseAbilities();
	ourVampire.InitAdvantages();
	
	////////////////////////////////////////////////////////////
	///////////////// Retrieve values from file  ///////////////
	////////////////////////////////////////////////////////////
	
	ourVampire.readFileNatures();
	ourVampire.readFileClans();
	
	///////////////// PLAYER NAME ////////////////////////
	
	scrollPlayerName.add(TViewPlayerName); // text view inside scroll
	scrollPlayerName.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	boxPlayerName.pack_start(scrollPlayerName); // scroll inside box
	fillBuffersPlayerName();
	on_button_bufferPlayerName();
	framePlayerName.add(boxPlayerName);
	mainGrid.attach(framePlayerName,0,0,6,1);
	
	///////////////// END PLAYER NAME ////////////////////////


	///////////////// NATURE ////////////////////////
	set<string> allNatures = ourVampire.getNatures();
	createComboBOX("Choice a nature:", allNatures, comboBoxNature, textChoiceNature, boxNature, buttonNature, "Submit Nature", frameNature, "NATURE", mainGrid, 1);
    // Action for button
    buttonNature.signal_clicked().connect(sigc::mem_fun(*this,&FirstInterface::choiceNature)); // when we click
    
	///////////////// END NATURE ////////////////////////


	///////////////// CLAN ////////////////////////
	set<string> allClans = ourVampire.getClans();
	createComboBOX("Choice a clan:", allClans, comboBoxCLAN, textChoiceCLAN, boxClan, buttonCLAN, "Submit Clan", frameClan, "CLAN", mainGrid, 2);
	
    // Action for button
    buttonCLAN.signal_clicked().connect(sigc::mem_fun(*this,&FirstInterface::choiceCLAN)); // when we click
	
	///////////////// END CLAN ////////////////////////


	///////////////// DESCRIPTION CLAN ////////////////////////
	scrollDescription.add(TViewDescription); // text view inside scroll
	scrollDescription.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	boxDescription.pack_start(scrollDescription); // scroll inside box
	frameDescription.set_label("ABOUT THIS CLAN");
	frameDescription.add(boxDescription);
	mainGrid.attach(frameDescription,1,2,5,1);
	///////////////// END DESCRIPTION CLAN ////////////////////////
	
	
	///////////////// DESCRIPTION NATURE ////////////////////////
	scrollDescriptionN.add(TViewDescriptionN); // text view inside scroll
	scrollDescriptionN.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	boxDescriptionN.pack_start(scrollDescriptionN); // scroll inside box
	frameDescriptionN.set_label("ABOUT THIS NATURE");
	frameDescriptionN.add(boxDescriptionN);
	mainGrid.attach(frameDescriptionN,1,1,5,1);
	///////////////// END DESCRIPTION CLAN ////////////////////////
	

	///////////////// FREEBIE POINTS ////////////////////////
	set<string> allChoicesFreebies;
	allChoicesFreebies.insert("Attributes - 5 per dot");
	allChoicesFreebies.insert("Ability - 2 per dot");
	allChoicesFreebies.insert("Discipline - 7 per dot");
	allChoicesFreebies.insert("Background - 1 per dot");
	allChoicesFreebies.insert("Virtue - 2 per dot");
	allChoicesFreebies.insert("Humanity - 2 per dot");
	allChoicesFreebies.insert("Willpower - 1 per dot");	
	
	// Add combobox
	createComboBOX("Choice a clan:", allChoicesFreebies, comboBoxFreebie, textChoiceFreebie, boxFreebie, buttonFreebie, "Submit Freebie points", frameFreebie, "How distribute freebie points ?", mainGrid, 3);
	
	buttonResetFreebie.set_label("RESET Freebies");
	boxFreebie.pack_start(buttonResetFreebie);	// Add button to reset choices
	
    // Action for buttons
    buttonFreebie.signal_clicked().connect(sigc::mem_fun(*this,&FirstInterface::distributeFreebies)); // when we click
    buttonResetFreebie.signal_clicked().connect(sigc::mem_fun(*this,&FirstInterface::resetFreebies)); // when we click
	
	///////////////// END FREEBIE POINTS ////////////////////////
	
	/////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// ATTRIBUTES ///////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////

	// Points we have to distribute between Strength/Dexterity/Stamina
    ComboBoxAttributes.append("Physical: 7 / Social: 5 / Mental: 3");
    ComboBoxAttributes.append("Physical: 7 / Social: 3 / Mental: 5");
    ComboBoxAttributes.append("Physical: 5 / Social: 7 / Mental: 3");
    ComboBoxAttributes.append("Physical: 5 / Social: 3 / Mental: 7");
    ComboBoxAttributes.append("Physical: 3 / Social: 7 / Mental: 5");
    ComboBoxAttributes.append("Physical: 3 / Social: 5 / Mental: 7");
    ComboBoxAttributes.set_active(0);
    
    // Submit button to distribute points of Attributes
    submitAttributes.add_label("Distribute Attributes");
	submitAttributes.signal_clicked().connect(sigc::mem_fun(*this,&FirstInterface::distributeAttributes));
	
	// Insert into Box Frame and Grid
	boxChoiceAttributes.pack_start(ComboBoxAttributes);
	boxChoiceAttributes.pack_start(submitAttributes);
	boxChoiceAttributes.pack_start(labelAttributes);
	frameAttributes.set_label("ATTRIBUTES - How many points for Physical, Social and Mental ?");
	frameAttributes.add(boxChoiceAttributes); 
	mainGrid.attach(frameAttributes,0,4,1,1);

	////////////////////////////////////////
	/////////////// PHYSICAL ///////////////
	////////////////////////////////////////

	// Commun button for Physical
    buttonPhysical.add_label("Submit Physical");
    buttonPhysical.set_size_request(100,30);
    
	///////////////// STRENGTH ////////////////////////
	createSpinButton(spinStrength, boxStrength, frameStrength, "PHYSICAL - Strength", mainGrid, 0, 5, textChoiceStrength);
	///////////////// END STRENGTH ////////////////////////

	///////////////// DEXTERITY ////////////////////////
	createSpinButton(spinDexterity, boxDexterity, frameDexterity, "PHYSICAL - Dexterity", mainGrid, 0, 6, textChoiceDexterity);
	///////////////// END DEXTERITY ////////////////////////
	
	///////////////// STAMINA ////////////////////////
	createSpinButton(spinStamina, boxStamina, frameStamina, "PHYSICAL - Stamina", mainGrid, 0, 7, textChoiceStamina);
	

	// when we click on button, execute choiceAttributes function
    buttonPhysical.signal_clicked().connect(sigc::bind<string, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, 
														Gtk::Label&, Gtk::Label&, Gtk::Label&>(sigc::mem_fun(*this,&FirstInterface::choiceAttributes),
																												"Physical", spinStrength, spinDexterity, spinStamina, // spinbuttons
																												textChoiceStrength, textChoiceDexterity, textChoiceStamina)); // and their labels

	
	///////////////// END STAMINA ////////////////////////
	
	// Add to grid
	mainGrid.attach(buttonPhysical,0,8,1,1);
	
	////////////////////////////////////////
	//////////// END PHYSICAL //////////////
	////////////////////////////////////////




	////////////////////////////////////////
	/////////////// SOCIAL /////////////////
	////////////////////////////////////////

    
	// Commun button for Social
    buttonSocial.add_label("Submit Social");
    buttonSocial.set_size_request(100,30);
    
	///////////////// CHARISMA ////////////////////////
	createSpinButton(spinCharisma, boxCharisma, frameCharisma, "SOCIAL - Charisma", mainGrid, 1, 5, textChoiceCharisma);
	///////////////// END CHARISMA ////////////////////////
	
	///////////////// MANIPULATION ////////////////////////
	createSpinButton(spinManipulation, boxManipulation, frameManipulation, "SOCIAL - Manipulation", mainGrid, 1, 6, textChoiceManipulation);
	///////////////// END MANIPULATION ////////////////////////
	
	///////////////// APPEARANCE ////////////////////////	
	createSpinButton(spinAppearance, boxAppearance, frameAppearance, "SOCIAL - Appearance", mainGrid, 1, 7, textChoiceAppearance);
	///////////////// END APPEARANCE ////////////////////////

	// when we click on button, execute choiceAttributes function
    buttonSocial.signal_clicked().connect(sigc::bind<string, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, 
														Gtk::Label&, Gtk::Label&, Gtk::Label&>(sigc::mem_fun(*this,&FirstInterface::choiceAttributes),
																												"Social", spinCharisma, spinManipulation, spinAppearance,
																												textChoiceCharisma, textChoiceManipulation, textChoiceAppearance));
	// Add to grid
	mainGrid.attach(buttonSocial,1,8,1,1);
	
	////////////////////////////////////////
	/////////// END SOCIAL /////////////////
	////////////////////////////////////////



	////////////////////////////////////////
	/////////////// MENTAL /////////////////
	////////////////////////////////////////
	
	// Commun button for Mental
    buttonMental.add_label("Submit Mental");
    buttonMental.set_size_request(100,30);
	
	///////////////// PERCEPTION ////////////////////////	
	createSpinButton(spinPerception, boxPerception, framePerception, "MENTAL - Perception", mainGrid, 2, 5, textChoicePerception);
	///////////////// END PERCEPTION ////////////////////////
	

	///////////////// INTELLIGENCE ////////////////////////	
	createSpinButton(spinIntelligence, boxIntelligence, frameIntelligence, "MENTAL - Intelligence", mainGrid, 2, 6, textChoiceIntelligence);
	///////////////// END INTELLIGENCE ////////////////////////

	///////////////// WITS ////////////////////////	
	createSpinButton(spinWits, boxWits, frameWits, "MENTAL - Wits", mainGrid, 2, 7, textChoiceWits);
	///////////////// END WITS ////////////////////////
	
	// when we click on button, execute choiceAttributes function
    buttonMental.signal_clicked().connect(sigc::bind<string, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, 
														Gtk::Label&, Gtk::Label&, Gtk::Label&>(sigc::mem_fun(*this,&FirstInterface::choiceAttributes),
																												"Mental", spinPerception, spinIntelligence, spinWits,
																												textChoicePerception, textChoiceIntelligence, textChoiceWits));

	// Add to grid
	mainGrid.attach(buttonMental,2,8,1,1);
	
	////////////////////////////////////////
	/////////// END MENTAL /////////////////
	////////////////////////////////////////	
	
	
	/////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////// END ATTRIBUTES /////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////



	/////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// ABILITIES ////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////

	// Points we have to distribute between Talents/Skills/Knowledges
    ComboBoxAbilities.append("Talents: 13 / Skills: 9 / Knowledges: 5");
    ComboBoxAbilities.append("Talents: 13 / Skills: 5 / Knowledges: 9");
    ComboBoxAbilities.append("Talents: 9 / Skills: 13 / Knowledges: 5");
    ComboBoxAbilities.append("Talents: 9 / Skills: 5 / Knowledges: 13");
    ComboBoxAbilities.append("Talents: 5 / Skills: 13 / Knowledges: 9");
    ComboBoxAbilities.append("Talents: 5 / Skills: 9 / Knowledges: 13");
    ComboBoxAbilities.set_active(0);
    
    // Submit button to distribute points of Abilities
    submitAbilities.add_label("Distribute Abilities");
	submitAbilities.signal_clicked().connect(sigc::mem_fun(*this,&FirstInterface::distributeAbilities));
	
	// Insert into Box Frame and Grid
	boxChoiceAbilities.pack_start(ComboBoxAbilities);
	boxChoiceAbilities.pack_start(submitAbilities);
	boxChoiceAbilities.pack_start(labelAbilities);
	frameAbilities.set_label("ABILITIES - How many points for Talents, Skills and Knowledges ?");
	frameAbilities.add(boxChoiceAbilities); 
	mainGrid.attach(frameAbilities,0,9,1,1);
	
	/// TALENTS
	createSpinButton(spinAlertness, boxAlertness, frameAlertness, "TALENTS - Alertness", mainGrid, 0, 10, textChoiceAlertness);
	createSpinButton(spinAthletics, boxAthletics, frameAthletics, "TALENTS - Athletics", mainGrid, 0, 11, textChoiceAthletics);
	createSpinButton(spinAwareness, boxAwareness, frameAwareness, "TALENTS - Awareness", mainGrid, 0, 12, textChoiceAwareness);
	createSpinButton(spinBrawl, boxBrawl, frameBrawl, "TALENTS - Brawl", mainGrid, 0, 13, textChoiceBrawl);
	createSpinButton(spinEmpathy, boxEmpathy, frameEmpathy, "TALENTS - Brawl", mainGrid, 0, 14, textChoiceEmpathy);
	createSpinButton(spinExpression, boxExpression, frameExpression, "TALENTS - Expression", mainGrid, 0, 15, textChoiceExpression);
	createSpinButton(spinIntimidation, boxIntimidation, frameIntimidation, "TALENTS - Intimidation", mainGrid, 0, 16, textChoiceIntimidation);
	createSpinButton(spinLeadership, boxLeadership, frameLeadership, "TALENTS - Leadership", mainGrid, 0, 17, textChoiceLeadership);
	createSpinButton(spinStreetwise, boxStreetwise, frameStreetwise, "TALENTS - Streetwise", mainGrid, 0, 18, textChoiceStreetwise);
	createSpinButton(spinSubterfuge, boxSubterfuge, frameSubterfuge, "TALENTS - Subterfuge", mainGrid, 0, 19, textChoiceSubterfuge);

	// Button submit Talents
	buttonTalents.add_label("Submit Talents");
	// when we click on button, execute choiceAbilities function
    buttonTalents.signal_clicked().connect(sigc::bind<string, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&,
																Gtk::SpinButton&>(sigc::mem_fun(*this,&FirstInterface::choiceAbilities),
																												"Talents", spinAlertness, spinAthletics, spinAwareness, spinBrawl, spinEmpathy,
																												spinExpression)); // spinbuttons
																												 // and their labels
	mainGrid.attach(buttonTalents,0,20,1,1);

	/// SKILLS
	createSpinButton(spinAnimalKen, boxAnimalKen, frameAnimalKen, "SKILLS - AnimalKen", mainGrid, 1, 10, textChoiceAnimalKen);
	createSpinButton(spinCrafts, boxCrafts, frameCrafts, "SKILLS - Crafts", mainGrid, 1, 11, textChoiceCrafts);
	createSpinButton(spinDrive, boxDrive, frameDrive, "SKILLS - Drive", mainGrid, 1, 12, textChoiceDrive);
	createSpinButton(spinEtiquette, boxEtiquette, frameEtiquette, "SKILLS - Etiquette", mainGrid, 1, 13, textChoiceEtiquette);
	createSpinButton(spinFirearms, boxFirearms, frameFirearms, "SKILLS - Firearms", mainGrid, 1, 14, textChoiceFirearms);
	createSpinButton(spinLarceny, boxLarceny, frameLarceny, "SKILLS - Larceny", mainGrid, 1, 15, textChoiceLarceny);
	createSpinButton(spinMelee, boxMelee, frameMelee, "SKILLS - Melee", mainGrid, 1, 16, textChoiceMelee);
	createSpinButton(spinPerformance, boxPerformance, framePerformance, "SKILLS - Performance", mainGrid, 1, 17, textChoicePerformance);
	createSpinButton(spinStealth, boxStealth, frameStealth, "SKILLS - Stealth", mainGrid, 1, 18, textChoiceStealth);
	createSpinButton(spinSurvival, boxSurvival, frameSurvival, "SKILLS - Survival", mainGrid, 1, 19, textChoiceSurvival);
	
	// Button submit Skills
	buttonSkills.add_label("Submit Skills");
	buttonSkills.signal_clicked().connect(sigc::bind<string, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&>(sigc::mem_fun(*this,&FirstInterface::choiceAbilities),
																												"Skills", spinAnimalKen, spinCrafts, spinDrive, spinEtiquette, spinFirearms, spinLarceny)); // and their label

	mainGrid.attach(buttonSkills,1,20,1,1);
	
	/// KNOWLEDGES
	createSpinButton(spinAcademics, boxAcademics, frameAcademics, "KNOWLEDGES - Academics", mainGrid, 2, 10, textChoiceAcademics);
	createSpinButton(spinComputer, boxComputer, frameComputer, "KNOWLEDGES - Computer", mainGrid, 2, 11, textChoiceComputer);
	createSpinButton(spinFinance,  boxFinance, frameFinance, "KNOWLEDGES - Finance", mainGrid, 2, 12, textChoiceFinance);
	createSpinButton(spinInvestigation, boxInvestigation, frameInvestigation, "KNOWLEDGES - Investigation", mainGrid, 2, 13, textChoiceInvestigation);
	createSpinButton(spinLaw, boxLaw, frameLaw, "KNOWLEDGES - Law", mainGrid, 2, 14, textChoiceLaw);
	createSpinButton(spinMedecine, boxMedecine, frameMedecine, "KNOWLEDGES - Medecine", mainGrid, 2, 15, textChoiceMedecine);
	createSpinButton(spinOccult, boxOccult, frameOccult, "KNOWLEDGES - Occult", mainGrid, 2, 16, textChoiceOccult);
	createSpinButton(spinPolitics, boxPolitics, framePolitics, "KNOWLEDGES - Politics", mainGrid, 2, 17, textChoicePolitics);
	createSpinButton(spinScience, boxScience, frameScience, "KNOWLEDGES - Science", mainGrid, 2, 18, textChoiceScience);
	createSpinButton(spinTechnology, boxTechnology, frameTechnology, "KNOWLEDGES - Technology", mainGrid, 2, 19, textChoiceTechnology);


	// Add button submit knowledges
	buttonKnowledges.add_label("Submit Knowledges");
	buttonKnowledges.signal_clicked().connect(sigc::bind<string, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&>(sigc::mem_fun(*this,&FirstInterface::choiceAbilities),
																												"Knowledges", spinAcademics, spinComputer, spinFinance, spinInvestigation, spinLaw, spinMedecine)); // and their label

	mainGrid.attach(buttonKnowledges,2,20,1,1);
	
	//createSpinButton(spin, textChoice, box, frame, "SKILLS - ", mainGrid, 0, 16, textChoice);


	/////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////// END ABILITIES ////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	///////////////// DISCIPLINES ////////////////////////
	createSpinButton(spinD1, boxD1, frameD1, "DISCIPLINE 1", mainGrid, 0, 21, textChoiceD1);	
	createSpinButton(spinD2, boxD2, frameD2, "DISCIPLINE 2", mainGrid, 1, 21, textChoiceD2);	
	createSpinButton(spinD3, boxD3, frameD3, "DISCIPLINE 3", mainGrid, 2, 21, textChoiceD3);	
	createSpinButton(spinD4, boxD4, frameD4, "DISCIPLINE 4", mainGrid, 3, 21, textChoiceD4);	
	createSpinButton(spinD5, boxD5, frameD5, "DISCIPLINE 5", mainGrid, 4, 21, textChoiceD5);	
	createSpinButton(spinD6, boxD6, frameD6, "DISCIPLINE 6", mainGrid, 5, 21, textChoiceD6);	
	
	// button into grid
	submitDisciplines.set_label("Submit Disciplines");
	mainGrid.attach(submitDisciplines,0,22,6,1);
	///////////////// END DISCIPLINES ////////////////////////





    // Add the Final button of the 1st interface
    buttonQuit.add_label("Create a vampire");
    buttonQuit.set_size_request(300,50);
    buttonQuit.signal_clicked().connect(sigc::mem_fun(*this,&FirstInterface::openNewWindow));
    mainGrid.attach(buttonQuit,0,23,6,1); // third argument = size of button (length)

	
	// Expand mainGrid, fill the window
	for (const auto &child : mainGrid.get_children()) {
		child->set_hexpand(true);
		child->set_halign(Gtk::ALIGN_FILL);
		child->set_vexpand(true);
		child->set_valign(Gtk::ALIGN_FILL);
	}

	// Add scroll
	SCROLL.set_border_width(10);
	SCROLL.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);
	add(SCROLL);
	// pack the grid into the scrolled window
	SCROLL.add(mainGrid);

    // Display the main grid in the main window
    mainGrid.set_column_spacing(10);
    mainGrid.set_row_spacing(10);
    //mainGrid.show_all();
    //add(mainGrid);
    show_all_children();
    
}


// Destructor of the class
FirstInterface::~FirstInterface() {}





void FirstInterface::fillBuffersPlayerName()
{
	textBufferPlayerName = Gtk::TextBuffer::create();
	textBufferPlayerName->set_text("WELCOME Player ! Please firstly give your name here. Then, enjoy !");
}

void FirstInterface::on_button_bufferPlayerName()
{
	TViewPlayerName.set_buffer(textBufferPlayerName);
}

void FirstInterface::fillBuffersDescription(string textDescriptionCLAN)
{
	textBufferDescription = Gtk::TextBuffer::create();
	textBufferDescription->set_text(textDescriptionCLAN);
}

void FirstInterface::on_button_bufferDescription()
{
	TViewDescription.set_buffer(textBufferDescription);
}

void FirstInterface::fillBuffersDescriptionN(string textDescriptionNATURE)
{
	textBufferDescriptionN = Gtk::TextBuffer::create();
	textBufferDescriptionN->set_text(textDescriptionNATURE);
}

void FirstInterface::on_button_bufferDescriptionN()
{
	TViewDescriptionN.set_buffer(textBufferDescriptionN);
}





/********************************************** CREATE COMBO BOXES **********************************************************/
/** This function creates combobox
 * @param label : label for display informations about combobox
 * @param text : text inside this label
 * @param infoToDisplay : words inside combobox
 * @param combobox: widget creating
 * @param textChoice: sentence to display choice of player
 * @param button: widget for submit
 * @param labelButton: text inside button
 * @param frame
 * @param text of frame
 * @param grid
 * @param where we want to place the widget
 * @return NOTHING */
 
void FirstInterface::createComboBOX(string text, set<string> infoToDisplay, Gtk::ComboBoxText& comboBox, 
										Gtk::Label& textChoice, Gtk::Box& box, Gtk::Button& button, string labelButton,
										Gtk::Frame& frame, string textFrame, Gtk::Grid& mainGrid, int positionLine) // to avoid repeative lines
{
  	//Fill the combobox to choice value
  	set<string>::iterator it;
	for (it=infoToDisplay.begin(); it!=infoToDisplay.end(); ++it) // Iterate till the end of set
		comboBox.append(*it);
	comboBox.set_active(0);
	    
	// Add a label for display choice of player
    textChoice.set_size_request(10,10);
    textChoice.set_alignment(Gtk::ALIGN_END);

    // Add submit button
	button.add_label(labelButton);
    button.set_size_request(100,30);
    
	// Add all composants into box
	box.set_spacing(30);
	box.pack_start(comboBox);
	box.pack_start(textChoice);
    box.pack_start(button);
	
	// Add box into frame
	frame.set_label(textFrame);
	frame.add(box);
	
	// Add frame to Grid
	mainGrid.attach(frame,0,positionLine,1,1); //0,1,1,1);
}







/********************************************** CREATE SPIN BUTTON **********************************************************/
void FirstInterface::createSpinButton(Gtk::SpinButton& spin, Gtk::Box& box, Gtk::Frame& frame, string textFrame, 
										Gtk::Grid& mainGrid, int positionFrameCol, int positionFrameLine,
										Gtk::Label& textAfterChoice) // to avoid repeative lines
/** This function creates spinbutton
 * @param spin : spin we want to creat
 * @param box
 * @param frame
 * @param textFrame: title of frame
 * @param mainGrid
 * @param positionFramCol: which column we want to display the spin button
 * @param positionFramLine: which line we want to display the spin button
 * @param textAfterChoice:display choice of player currently
 * @return NOTHING */										
{
    // Add the spin button
    spin.set_range(0, 0); // minimum and maximum initialization
    spin.set_value(0);
    spin.set_increments(1,1);

	// Add a label for display choice of player
    textAfterChoice.set_size_request(10,10);
    textAfterChoice.set_alignment(Gtk::ALIGN_END);
    
    // Add spinbutton in box
    box.set_spacing(30);
    box.pack_start(spin);
    box.pack_start(textAfterChoice);

    // Add box in frame
    frame.set_label(textFrame);
    frame.add(box);
    
	// Add frame in Grid
	mainGrid.attach(frame,positionFrameCol,positionFrameLine,1,1);
	
}






/********************************************** AFTER CLICK "Submit Nature" BUTTON **********************************************************/
// Call when the button of Nature is called
void FirstInterface::choiceNature()
/** This function executes after we call the Nature button
 * Display choices given by player and registers in Vampire object the value
 * @return NOTHING */	
{   
	string valueNature = comboBoxNature.get_active_text ();
	string textLabel = "You are chosen " + valueNature + " Nature.";
	textChoiceNature.set_text(textLabel);
	
	// Input value on ourVampire object
	ourVampire.SetNature_and_Demeanor(valueNature); // register name of nature in the object ourVampire
	
	// Retrieve description of clan	and display in text view
	string descriptionN = ourVampire.getDescriptionNature(valueNature); // method from class File
	fillBuffersDescriptionN(descriptionN);
	on_button_bufferDescriptionN();
	
	// Execute disciplines spinbutton
	choiceDisciplines();
}





/********************************************** AFTER CLICK "Submit Clan" BUTTON **********************************************************/
// Call when the button of CLAN is clicked
void FirstInterface::choiceCLAN()
/** This function executes after we call the Clan button
 * Display choices given by player and registers in Vampire object the value
 * According clan give, function change appearance for one clan, and selects then displays disciplines of this clan
 * @return NOTHING */	
{   
	string valueCLAN = comboBoxCLAN.get_active_text ();
	string textLabel = "You are chosen " + valueCLAN + " Clan.";
	textChoiceCLAN.set_text(textLabel);
	
	// Input values on ourVampire object
	ourVampire.SetClanName(valueCLAN); // register name of clan in the object ourVampire
	ourVampire.InitialiseAttribute(valueCLAN); 
	

	// If the clan is Nosferatu, appearance = 0
	string clan = ourVampire.GetClanName();
	int initialMaximumSocial = ourVampire.getSocialPoints(); // retrieve maximum points for Social
	if 	(clan == "Nosferatu")
	{
		spinAppearance.hide();
	}
	if (clan != "Nosferatu")
	{
		spinAppearance.show();
	}	
	
	// Retrieve nickname of clan
	string nickname = ourVampire.getNickname(clan);
	
	// Retrieve description of clan	and display in text view
	string description = ourVampire.getDescriptionClan(clan);
	fillBuffersDescription(description);
	on_button_bufferDescription();
	
	/// DISCIPLINES
	// Retrieve name of disciplines
	vector<string> disciplinesGiven = ourVampire.getDiscipline(clan);	

	int sizeVector = disciplinesGiven.size();
	for(int i = sizeVector; i<6; i++)
	{
		disciplinesGiven.push_back("NOT AVAILABLE"); // add label for spinbutton without discipline
	}

	frameD1.set_label("DISCIPLINE "+disciplinesGiven[0]);
	frameD2.set_label("DISCIPLINE "+disciplinesGiven[1]);	
	frameD3.set_label("DISCIPLINE "+disciplinesGiven[2]);	
	frameD4.set_label("DISCIPLINE "+disciplinesGiven[3]);	
	frameD5.set_label("DISCIPLINE "+disciplinesGiven[4]);	
	frameD6.set_label("DISCIPLINE "+disciplinesGiven[5]);	
	
	// Active button
	choiceDisciplines();
		
	if (frameD1.get_label().compare("DISCIPLINE NOT AVAILABLE") == 0) {spinD1.hide();} else {spinD1.show();} ;
	if (frameD2.get_label().compare("DISCIPLINE NOT AVAILABLE") == 0) {spinD2.hide();} else {spinD2.show();} ;
	if (frameD3.get_label().compare("DISCIPLINE NOT AVAILABLE") == 0) {spinD3.hide();} else {spinD3.show();} ;
	if (frameD4.get_label().compare("DISCIPLINE NOT AVAILABLE") == 0) {spinD4.hide();} else {spinD4.show();} ;
	if (frameD5.get_label().compare("DISCIPLINE NOT AVAILABLE") == 0) {spinD5.hide();} else {spinD5.show();} ;
	if (frameD6.get_label().compare("DISCIPLINE NOT AVAILABLE") == 0) {spinD6.hide();} else {spinD6.show();} ;
	
	submitDisciplines.signal_clicked().connect(sigc::mem_fun(*this,&FirstInterface::choiceDisciplines));
}



/********************************************** AFTER CLICK "Distribute Freebies" BUTTON **********************************************************/
void FirstInterface::distributeFreebies()
/** This function distributes 15 free points between Attributes/ Ability/ Discipline etc
 * Decrementes totals points after submit points
 * and displays choices given by player
 * @return NOTHING */	

{
	// Retrieve current value in vampire object
	int nbPointsFree = ourVampire.setFreePoints();
	
	// final string to display in interface
	string finalText = "";
	
	string valueComboBox = comboBoxFreebie.get_active_text (); // retrieve of value given in combobox
	if (valueComboBox == "Attributes - 5 per dot" && nbPointsFree > 0 && nbPointsFree - 5 >= 0)
		{
			nbPointsFree = nbPointsFree - 5;			
			ourVampire.changeFreePoints(nbPointsFree, "Attributes");
			// Activate spinbuttons
			//choiceAttributes();
		}
	if (valueComboBox == "Ability - 2 per dot" && nbPointsFree > 0 && nbPointsFree - 2 >= 0)
		{
			nbPointsFree = nbPointsFree - 2;			
			ourVampire.changeFreePoints(nbPointsFree, "Ability");
			// Activate spinbuttons
			//choiceAbilities();
		}
	if (valueComboBox == "Discipline - 7 per dot" && nbPointsFree > 0 && nbPointsFree - 7 >= 0)
		{
			nbPointsFree = nbPointsFree - 7;
			ourVampire.changeFreePoints(nbPointsFree, "Discipline");
			// Activate spinbuttons
			string clan = ourVampire.GetClanName();
			if (clan != "")
			{
				choiceDisciplines();
			} else
			{
				finalText+="\nPLEASE SELECT A CLAN!";
			}

		}
	if (valueComboBox == "Background - 1 per dot" && nbPointsFree > 0 && nbPointsFree - 1 >= 0)
		{
			//
		}
	if (valueComboBox == "Virtue - 2 per dot" && nbPointsFree > 0 && nbPointsFree - 2 >= 0)
		{
			//
		}	
	if (valueComboBox == "Humanity - 2 per dot" && nbPointsFree > 0 && nbPointsFree - 2 >= 0)
		{
			//
		}	
	if (valueComboBox == "Willpower - 1 per dot" && nbPointsFree > 0 && nbPointsFree - 1 >= 0)
		{
			//
		}
	
	finalText+= "\nIt remains " + to_string(nbPointsFree) + " points to distribute. \nYou have chosen ";
	finalText+= to_string(ourVampire.getAttributesFree()) + " additional points for Attributes, ";
	finalText+= to_string(ourVampire.getAbilityFree()) + " for Ability, and " + to_string(ourVampire.getDisciplinesFree()) + " for Disciplines.";
	
	textChoiceFreebie.set_text(finalText);
	
}



/********************************************** AFTER CLICK "RESET Freebies" BUTTON **********************************************************/
void FirstInterface::resetFreebies()
/** This function resets free points, all things doing previously in distributeFreebie()
 * @return NOTHING */
{
	ourVampire.InitAdvantages();
	textChoiceFreebie.set_text("You reset all, distribute again 15 points.");
}





/********************************************** AFTER CLICK "Distribute Attributes" BUTTON **********************************************************/
void FirstInterface::distributeAttributes()
/** This function distribute points between Physical Social and Mental, and registers values in vampire object
 * if clan is absent, function display that we need to choose a clan
 * @return NOTHING */
{
	int initialMaximumPhysical; int initialMaximumSocial; int initialMaximumMental; string valueComboBox; // initialization
	valueComboBox = ComboBoxAttributes.get_active_text (); // retrieve of value given
	if (valueComboBox == "Physical: 7 / Social: 5 / Mental: 3")
		{initialMaximumPhysical = 7; initialMaximumSocial = 5; initialMaximumMental = 3;}
	if (valueComboBox == "Physical: 7 / Social: 3 / Mental: 5")
		{initialMaximumPhysical = 7; initialMaximumSocial = 3; initialMaximumMental = 5;}
	if (valueComboBox == "Physical: 5 / Social: 7 / Mental: 3")
		{initialMaximumPhysical = 5; initialMaximumSocial = 7; initialMaximumMental = 3;}
	if (valueComboBox == "Physical: 5 / Social: 3 / Mental: 7")
		{initialMaximumPhysical = 5; initialMaximumSocial = 3; initialMaximumMental = 7;}
	if (valueComboBox == "Physical: 3 / Social: 7 / Mental: 5")
		{initialMaximumPhysical = 3; initialMaximumSocial = 7; initialMaximumMental = 5;}
	if (valueComboBox == "Physical: 3 / Social: 5 / Mental: 7")
		{initialMaximumPhysical = 3; initialMaximumSocial = 5; initialMaximumMental = 7;}	
		
	// Add additionnal points from Freebies Points
	int additionalPoints = ourVampire.getAttributesFree();
	initialMaximumPhysical+= additionalPoints;
	initialMaximumSocial+= additionalPoints;
	initialMaximumMental+= additionalPoints;
		
	// Register values chosen by player inside object ourVampire
	ourVampire.setPhysicalPoints(initialMaximumPhysical);
	ourVampire.setSocialPoints(initialMaximumSocial);
	ourVampire.setMentalPoints(initialMaximumMental);
	
	// Impossible to give value > 8
	if ( initialMaximumPhysical>8 ) { initialMaximumPhysical=8; };
	if( initialMaximumSocial>8) { initialMaximumSocial=8; };
	if( initialMaximumMental>8 ) { initialMaximumMental=8; };
	
	// Activate clan only if we submit a name clan
	string clan = ourVampire.GetClanName();
	if (clan != "")
	{
		// Change value of the maximum of each spinButon
		// PHYSICAL
		spinStrength.set_range(0,initialMaximumPhysical);
		spinDexterity.set_range(0,initialMaximumPhysical);
		spinStamina.set_range(0,initialMaximumPhysical);
		// SOCIAL    
		spinCharisma.set_range(0,initialMaximumSocial);
		spinManipulation.set_range(0,initialMaximumSocial);   
		if (clan != "Academics") { spinAppearance.set_range(0,initialMaximumSocial); }
		// MENTALS
		spinPerception.set_range(0,initialMaximumMental);
		spinIntelligence.set_range(0,initialMaximumMental);
		spinWits.set_range(0,initialMaximumMental);
	}
	else // write that player need to give a clan
	{
		labelAttributes.set_text("Give a clan please.");
	}

}




/********************************************** AFTER CLICK "Distribute Attributes" BUTTON **********************************************************/
void FirstInterface::distributeAbilities()
/** This function executing after submit how we want distribute scores inside Attributes
* retrieves values given of combobox, and saves it inside Vampire object
* then executes spinbuttons according choices
 * @return NOTHING */
{
	int initialMaximumTalents; int initialMaximumSkills; int initialMaximumKnowledges; string valueComboBox; // initialization
	valueComboBox = ComboBoxAbilities.get_active_text ();
	if (valueComboBox == "Talents: 13 / Skills: 9 / Knowledges: 5")
		{initialMaximumTalents = 13; initialMaximumSkills = 9; initialMaximumKnowledges = 5;}
	if (valueComboBox == "Talents: 13 / Skills: 5 / Knowledges: 9")
		{initialMaximumTalents = 13; initialMaximumSkills = 5; initialMaximumKnowledges = 9;}
	if (valueComboBox == "Talents: 9 / Skills: 13 / Knowledges: 5")
		{initialMaximumTalents = 9; initialMaximumSkills = 5; initialMaximumKnowledges = 5;}
	if (valueComboBox == "Talents: 9 / Skills: 5 / Knowledges: 13")
		{initialMaximumTalents = 9; initialMaximumSkills = 5; initialMaximumKnowledges = 13;}
	if (valueComboBox == "Talents: 5 / Skills: 13 / Knowledges: 9")
		{initialMaximumTalents = 5; initialMaximumSkills = 9; initialMaximumKnowledges = 9;}
	if (valueComboBox == "Talents: 5 / Skills: 9 / Knowledges: 13")
		{initialMaximumTalents = 5; initialMaximumSkills = 9; initialMaximumKnowledges = 13;}
		
	// Add additionnal points from Freebies Points
	int additionalPoints = ourVampire.getAbilityFree();
	
	initialMaximumTalents+= additionalPoints;
	initialMaximumSkills+= additionalPoints;
	initialMaximumKnowledges+= additionalPoints;
	
	// Register values chosen by player inside object ourVampire
	ourVampire.setTalentPoints(initialMaximumTalents);
	ourVampire.setSkillsPoints(initialMaximumSkills);
	ourVampire.setKnowledgesPoints(initialMaximumKnowledges);
		
	// Impossible to give value > 8
	if ( initialMaximumTalents>8 ) { initialMaximumTalents=8; };
	if( initialMaximumSkills>8) { initialMaximumSkills=8; };
	if( initialMaximumKnowledges>8 ) { initialMaximumKnowledges=8; };
		
	// Active spin buttons
	// Talents
	spinAlertness.set_range(0,initialMaximumTalents);
	spinAthletics.set_range(0,initialMaximumTalents);
	spinAwareness.set_range(0,initialMaximumTalents);
	spinBrawl.set_range(0,initialMaximumTalents);
	spinEmpathy.set_range(0,initialMaximumTalents);
	spinExpression.set_range(0,initialMaximumTalents);
	spinIntimidation.set_range(0,initialMaximumTalents);
	spinLeadership.set_range(0,initialMaximumTalents);
	spinStreetwise.set_range(0,initialMaximumTalents);
	spinSubterfuge.set_range(0,initialMaximumTalents);
	// Skills
	spinAnimalKen.set_range(0,initialMaximumSkills);
	spinCrafts.set_range(0,initialMaximumSkills);
	spinDrive.set_range(0,initialMaximumSkills);
	spinEtiquette.set_range(0,initialMaximumSkills);
	spinFirearms.set_range(0,initialMaximumSkills);
	spinLarceny.set_range(0,initialMaximumSkills);
	spinMelee.set_range(0,initialMaximumSkills);
	spinPerformance.set_range(0,initialMaximumSkills);
	spinStealth.set_range(0,initialMaximumSkills);
	spinSurvival.set_range(0,initialMaximumSkills);
	// Knowledges
	spinAcademics.set_range(0,initialMaximumKnowledges);
	spinComputer.set_range(0,initialMaximumKnowledges);
	spinFinance.set_range(0,initialMaximumKnowledges);
	spinInvestigation.set_range(0,initialMaximumKnowledges);
	spinLaw.set_range(0,initialMaximumKnowledges);
	spinMedecine.set_range(0,initialMaximumKnowledges);
	spinOccult.set_range(0,initialMaximumKnowledges);
	spinPolitics.set_range(0,initialMaximumKnowledges);
	spinScience.set_range(0,initialMaximumKnowledges);
	spinTechnology.set_range(0,initialMaximumKnowledges);

		
	// Register values chosen by player inside object ourVampire
	ourVampire.setTalentPoints(initialMaximumTalents);
	ourVampire.setSkillsPoints(initialMaximumSkills);
	ourVampire.setKnowledgesPoints(initialMaximumKnowledges);

}




/********************************************** AFTER CLICK "Submit [Physical, Social or Mental]" BUTTON **********************************************************/
/** This function distributes points inside an attribute: according freebies points given and distribution points of attributes given
 * dispays choices of player and displays if the distribution it's correct between Physical, Social and Mental or not
* @param name of attribute
* @param spin*3 : 3 spinButton to create
* @param textChoice*3 : label to display what choices the player has done
 * @return NOTHING */
void FirstInterface::choiceAttributes(string attribute, Gtk::SpinButton& spin1, Gtk::SpinButton& spin2, Gtk::SpinButton& spin3, 
										Gtk::Label& textChoice1, Gtk::Label& textChoice2, Gtk::Label& textChoice3)
										
{  
	int initialMaximum;
	
	// Retrieve value of maximum points to distribute given by player (see function distributeAttributes())
	if(attribute == "Physical"){ initialMaximum = ourVampire.getPhysicalPoints(); }
    if(attribute == "Social"){ initialMaximum = ourVampire.getSocialPoints(); }
	if(attribute == "Mental"){ initialMaximum = ourVampire.getMentalPoints(); }

	// Retrieve Current Value of each spinButton
	int currentValue1 = int(spin1.get_value()); // convert double to int
	int currentValue2 = int(spin2.get_value());
	int currentValue3 = int(spin3.get_value());
	
	
	// If player send points not correcty, too much points given
	int sumPointsGivenByPlayer = currentValue1 + currentValue2 + currentValue3;
	
	//////////////////////////////////////////////////////////
	// Retrieve additional points used by others attributes //
	
	int pointsFree = ourVampire.getAttributesFree(); // how many points free we have currently

    int additionalPointsUsed = 0;
    
    if(attribute == "Physical"){ 

        vector<int> v = ourVampire.getScoresSocial();
        int sumScoreCurrentSocial = v[0] + v[1] + v[2];
        int pointsNotFreebies = ourVampire.getSocialPoints() - pointsFree;
        int additionalPointsUsedCalculated = sumScoreCurrentSocial - pointsNotFreebies; // retrieve additionnal points already used currently
		if(additionalPointsUsedCalculated>=0)
		{
			additionalPointsUsed = additionalPointsUsedCalculated;
		} // we can have negative points, so we don't considere that, significate that payer not give much points
		
        vector<int> v1 = ourVampire.getScoresMental();
        int sumScoreCurrentMental = accumulate(v1.begin(),v1.end(),0);
        int additionalPointsUsedCalculated2= sumScoreCurrentMental - (ourVampire.getMentalPoints() - pointsFree);
		if(additionalPointsUsedCalculated2>=0)
		{
			additionalPointsUsed+= additionalPointsUsedCalculated2;
		}
        initialMaximum = initialMaximum - additionalPointsUsed; // remove freebies already used by other spinbutton, update maximun points authorized
    }
    
    if(attribute == "Social"){ 

        vector<int> v = ourVampire.getScoresPhysical();
        int sumScoreCurrentPhysical = v[0] + v[1] + v[2];
        int pointsNotFreebies = ourVampire.getPhysicalPoints() - pointsFree;
        int additionalPointsUsedCalculated = sumScoreCurrentPhysical - pointsNotFreebies; // retrieve additionnal points already used currently
		if(additionalPointsUsedCalculated>=0)
		{
			additionalPointsUsed = additionalPointsUsedCalculated;
		} // we can have negative points, so we don't considere that
		
        vector<int> v1 = ourVampire.getScoresMental();
        int sumScoreCurrentMental = accumulate(v1.begin(),v1.end(),0);
        int additionalPointsUsedCalculated2= sumScoreCurrentMental - (ourVampire.getMentalPoints() - pointsFree);
		if(additionalPointsUsedCalculated2>=0)
		{
			additionalPointsUsed+= additionalPointsUsedCalculated2;
		}
        initialMaximum = initialMaximum - additionalPointsUsed; // remove freebies already used by other spinbutton
    }
    
    if(attribute == "Mental"){ 

        vector<int> v = ourVampire.getScoresPhysical();
        int sumScoreCurrentPhysical = v[0] + v[1] + v[2];
        int pointsNotFreebies = ourVampire.getPhysicalPoints() - pointsFree;
        int additionalPointsUsedCalculated = sumScoreCurrentPhysical - pointsNotFreebies; // retrieve additionnal points already used currently
		if(additionalPointsUsedCalculated>=0)
		{
			additionalPointsUsed = additionalPointsUsedCalculated;
		} // we can have negative points, so we don't considere that
		
        vector<int> v1 = ourVampire.getScoresSocial();
        int sumScoreCurrentSocial = accumulate(v1.begin(),v1.end(),0);
        int pointsNotFreebies1 = ourVampire.getSocialPoints() - pointsFree;
        int additionalPointsUsedCalculated2 = sumScoreCurrentSocial - pointsNotFreebies1; // retrieve additionnal points already used currently
		if(additionalPointsUsedCalculated2>=0)
		{
			additionalPointsUsed+= additionalPointsUsedCalculated2;
		} // we can have negative points, so we don't considere that
		
        initialMaximum = initialMaximum - additionalPointsUsed; // remove freebies already used by other spinbutton

    }
    
	// END Retrieve additional points used by others attributes //
	//////////////////////////////////////////////////////////////

  

	if (sumPointsGivenByPlayer != initialMaximum || currentValue1 > 8 || currentValue2 > 8 || currentValue3 > 8){
		// Add labels for each spin button indicating errors
		string textLabel = "Not correctly, reset please!";
		textChoice1.set_text(textLabel);
		textChoice2.set_text(textLabel);
		textChoice3.set_text(textLabel);
		
		// Initialize all spins
		if (initialMaximum < 9)
		{
			spin1.set_range(0,initialMaximum);
			spin2.set_range(0,initialMaximum);
			spin3.set_range(0,initialMaximum);
		}
		else
		{
			spin1.set_range(0,8); // max of points for each spin = 8 points
			spin2.set_range(0,8);
			spin3.set_range(0,8);
		}
		
	} else {
		// Add a label for the each spin button
		string textLabel1 = "You have chosen " + to_string(currentValue1) + " point(s)."; // convert int to string
		string textLabel2 = "You have chosen " + to_string(currentValue2) + " point(s).";
		string textLabel3 = "You have chosen " + to_string(currentValue3) + " point(s).";
		textChoice1.set_text(textLabel1);
		textChoice2.set_text(textLabel2);
		textChoice3.set_text(textLabel3);
		
		// Change value of the maximum of each spinButon: Value of maximum for a current spinbutton = initial maximum value - sum(current value of others spin buttons)
		int newMaximum1 = initialMaximum - currentValue2 - currentValue3;
		spin1.set_range(0,newMaximum1);
		int newMaximum2 = initialMaximum - currentValue1 - currentValue3;
		spin2.set_range(0,newMaximum2);
		int newMaximum3 = initialMaximum - currentValue1 - currentValue2;
		spin3.set_range(0,newMaximum3);
	}

	// SAVE values in ourVampire object
	if(attribute == "Physical"){ ourVampire.distribPhysical(currentValue1, currentValue2, currentValue3);}
	if(attribute == "Social"){ ourVampire.distribSocial(currentValue1, currentValue2, currentValue3);}
	if(attribute == "Mental"){ ourVampire.distribMental(currentValue1, currentValue2, currentValue3);}

}




/********************************************** AFTER CLICK "Submit [Talents, Abilities or Knowledges]" BUTTON **********************************************************/
void FirstInterface::choiceAbilities(string abilitie, Gtk::SpinButton& spin1, Gtk::SpinButton& spin2, Gtk::SpinButton& spin3, Gtk::SpinButton& spin4, Gtk::SpinButton& spin5,
														Gtk::SpinButton& spin6)
/** This function distribute points inside an abilitie
* @param name of attribute
* @param spin*6 : 6 spinButton to create
 * @return NOTHING 
 * 
 *
 * PROBLEM : Je n'ai pas pu ajouter d'autres paramètres dans la fonction
 * error: no matching function for call to ‘mem_fun(FirstInterface&, void (FirstInterface::*)(std::string, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::Label&))’
  283 |                 Gtk::SpinButton&, Gtk::Label&>(sigc::mem_fun(*this,&FirstInterface::choiceAbilities),
 * 
 * note:   mismatched types ‘T_return (T_obj::*)()’ and ‘FirstInterface’
  283 |                 Gtk::SpinButton&, Gtk::Label&>(sigc::mem_fun(*this,&FirstInterface::choiceAbilities),
  * 
  * 
  * note:   types ‘T_return (T_obj2::)(T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6, T_arg7) const volatile’ and ‘void (FirstInterface::)(std::string, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::Label&)’ {aka ‘void (FirstInterface::)(std::__cxx11::basic_string<char>, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::Label&)’} have incompatible cv-qualifiers

 * */
{  
	int initialMaximum;
	
	// Retrieve value of maximum points to distribute given by player (see function distributeAttributes())
	if(abilitie == "Talents"){ initialMaximum = ourVampire.getTalentPoints(); }
    if(abilitie == "Skills"){ initialMaximum = ourVampire.getSkillsPoints(); }
	if(abilitie == "Knowledges"){ initialMaximum = ourVampire.getKnowledgesPoints(); }
	
	// Retrieve Current Value of each spinButton	
	int currentValues[6]; // Fill table:
	currentValues[0]=int(spin1.get_value());
	currentValues[1]=int(spin2.get_value());
	currentValues[2]=int(spin3.get_value());
	currentValues[3]=int(spin4.get_value());
	currentValues[4]=int(spin5.get_value());
	currentValues[5]=int(spin6.get_value());
	/*currentValues[int(spin7.get_value())];
	currentValues[int(spin8.get_value())];
	currentValues[int(spin9.get_value())];
	currentValues[int(spin10.get_value())];*/
	
	// Check that all value are <= 8 points
	bool checkMax = true;
	for(int i=0; i<6; i++)
	{
		if (currentValues[i] > 9)
			checkMax == false; // points have not spent correctly
	}
	

	// If player send points not correcty, too much points given
	int sumPointsGivenByPlayer = 0;
	for(int i=0; i<6; i++)
	{
		sumPointsGivenByPlayer += currentValues[i];
	}
		
	if (sumPointsGivenByPlayer != initialMaximum || checkMax == true){
		// Add labels for each spin button indicating errors
		string textLabel = "Not correctly, reset please!";
		/*textChoice1.set_text(textLabel);
		textChoice2.set_text(textLabel);
		textChoice3.set_text(textLabel);
		textChoice4.set_text(textLabel);
		textChoice5.set_text(textLabel);
		textChoice6.set_text(textLabel);
		textChoice7.set_text(textLabel);
		textChoice8.set_text(textLabel);
		textChoice9.set_text(textLabel);
		textChoice10.set_text(textLabel);*/
		
		// Initialize all
		spin1.set_range(0,initialMaximum);
		spin2.set_range(0,initialMaximum);
		spin3.set_range(0,initialMaximum);
		spin4.set_range(0,initialMaximum);
		spin5.set_range(0,initialMaximum);
		spin6.set_range(0,initialMaximum);
		/*spin7.set_range(0,initialMaximum);
		spin8.set_range(0,initialMaximum);
		spin9.set_range(0,initialMaximum);
		spin10.set_range(0,initialMaximum);*/
		
		
	} else {
		// Add a label for the each spin button
		//textChoice1.set_text("You have chosen " + to_string(currentValues[0]) + " point(s).");
		
		// Change value of the maximum of each spinButon: Value of maximum for a current spinbutton = initial maximum value - sum(current value of others spin buttons)
		int newMaximum1 = initialMaximum - sumPointsGivenByPlayer + currentValues[0];
		int newMaximum2 = initialMaximum - sumPointsGivenByPlayer + currentValues[1];
		int newMaximum3 = initialMaximum - sumPointsGivenByPlayer + currentValues[2];
		int newMaximum4 = initialMaximum - sumPointsGivenByPlayer + currentValues[3];
		int newMaximum5 = initialMaximum - sumPointsGivenByPlayer + currentValues[4];
		int newMaximum6 = initialMaximum - sumPointsGivenByPlayer + currentValues[5];

		spin1.set_range(0,newMaximum1);
		spin2.set_range(0,newMaximum2);
		spin3.set_range(0,newMaximum3);
		spin4.set_range(0,newMaximum4);
		spin5.set_range(0,newMaximum5);
		spin6.set_range(0,newMaximum6);
		/*spin7.set_range(0,newMaximum7);
		spin8.set_range(0,newMaximum8);
		spin9.set_range(0,newMaximum9);
		spin10.set_range(0,newMaximum10);*/
	}

	// SAVE values in ourVampire object
	if(abilitie == "Talents")
	{ 
		ourVampire.resetTalent(); // reset vector
		for(int i=0; i<6; i++)
		{
			ourVampire.setTalent(currentValues[i]);
		}
	}
	if(abilitie == "Skills")
	{ 
		ourVampire.resetSkills(); // reset vector
		for(int i=0; i<6; i++)
		{
			ourVampire.setSkills(currentValues[i]);
		}
	}
	if(abilitie == "Knowledges")
	{ 
		ourVampire.resetKnowledges(); // reset vector
		for(int i=0; i<6; i++)
		{
			ourVampire.setKnowledges(currentValues[i]);
		}
	}
}





/********************************************** AFTER CLICK "Submit Disciplines" or "Submit clan" BUTTON **********************************************************/
void FirstInterface::choiceDisciplines()
/** This function executing after submit Disciplines
 * controls spinbutton and check maximum authorized
 * @return NOTHING */
{
	
	int initialMaximum = 3; // maxium points to distribute
	
	int currentValues[6]; // Initialize a table
	
	// Retrieve Current Value of each spinButton, fill table
	currentValues[0] = int(spinD1.get_value());
	currentValues[1] = int(spinD2.get_value());
	currentValues[2] = int(spinD3.get_value());
	currentValues[3] = int(spinD4.get_value());
	currentValues[4] = int(spinD5.get_value());
	currentValues[5] = int(spinD6.get_value());
	
	// Add additionnal points from Freebies Points
	int additionalPoints = ourVampire.getDisciplinesFree();
	initialMaximum+= additionalPoints;
	
	// Initialization of spinButton, also after submit a clan
	spinD1.set_range(0,initialMaximum);
	spinD2.set_range(0,initialMaximum);
	spinD3.set_range(0,initialMaximum);
	spinD4.set_range(0,initialMaximum);
	spinD5.set_range(0,initialMaximum);
	spinD6.set_range(0,initialMaximum);	
	
	// Sum of points
	int sumPointsGivenByPlayer = 0;
	for(int i=0; i<6; i++)
	{
		sumPointsGivenByPlayer += currentValues[i];
	}
	
	// Check that all value are <= 8 points
	bool checkMax = true;
	for(int i=0; i<11; i++)
	{
		if (currentValues[i] >= 9)
			checkMax == false; // points have not spent correctly
	}
		
	// If player send points not correcty, too much points given
	if (sumPointsGivenByPlayer != initialMaximum || checkMax == true){
		// Add labels for each spin button indicating errors
		string textLabel = "Not correctly, reset please!";
		textChoiceD1.set_text(textLabel);
		textChoiceD2.set_text(textLabel);
		textChoiceD3.set_text(textLabel);
		textChoiceD4.set_text(textLabel);
		textChoiceD5.set_text(textLabel);
		textChoiceD6.set_text(textLabel);
		
		// UPDATE ALL
		spinD1.set_range(0,initialMaximum);
		spinD2.set_range(0,initialMaximum);
		spinD3.set_range(0,initialMaximum);
		spinD4.set_range(0,initialMaximum);
		spinD5.set_range(0,initialMaximum);
		spinD6.set_range(0,initialMaximum);
		
	} else {
		// Add a label for the each spin button	
		textChoiceD1.set_text("You have chosen " + to_string(currentValues[0]) + " point(s).");
		textChoiceD2.set_text("You have chosen " + to_string(currentValues[1]) + " point(s).");
		textChoiceD3.set_text("You have chosen " + to_string(currentValues[2]) + " point(s).");
		textChoiceD4.set_text("You have chosen " + to_string(currentValues[3]) + " point(s).");
		textChoiceD5.set_text("You have chosen " + to_string(currentValues[4]) + " point(s).");
		textChoiceD6.set_text("You have chosen " + to_string(currentValues[5]) + " point(s).");

		// Change value of the maximum of each spinButon: Value of maximum for a current spinbutton = initial maximum value - sum(current value of others spin buttons)
		int newMaximum1 = initialMaximum - sumPointsGivenByPlayer + currentValues[0];
		int newMaximum2 = initialMaximum - sumPointsGivenByPlayer + currentValues[1];
		int newMaximum3 = initialMaximum - sumPointsGivenByPlayer + currentValues[2];
		int newMaximum4 = initialMaximum - sumPointsGivenByPlayer + currentValues[3];
		int newMaximum5 = initialMaximum - sumPointsGivenByPlayer + currentValues[4];
		int newMaximum6 = initialMaximum - sumPointsGivenByPlayer + currentValues[5];

		spinD1.set_range(0,newMaximum1);
		spinD2.set_range(0,newMaximum2);
		spinD3.set_range(0,newMaximum3);
		spinD4.set_range(0,newMaximum4);
		spinD5.set_range(0,newMaximum5);
		spinD6.set_range(0,newMaximum6);
	}

	// SAVE values in ourVampire object
	for(int i=0; i<6; i++)
		{
			ourVampire.setDisciplines(i, currentValues[i]);
		}

}



/********************************************** CALCULATE EXPERIENCE **********************************************************/
void FirstInterface::calculateExperience()
/** This function calculate experience of vampire according some rules
 * 
 * RULES:
 * 	//New Ability:3
	//New Discipline:10
	//New Path:7
	//Attribute:CRx4
	//Ability:CRx2
	//Clan Discipline:CRx5
	//Other Discipline:CRx7
	//Secondary Path:CRx4
	//Virtue:CRx2
	//Humanity or Path of Enlightenment:CR2
	//Willpower:CR
	//	!!*CR is the current rating of the trait
 *
 * 
 * */
{	
	
	/// Attribute:CRx4
	// Physical
	vector<int> scoresPhysical = ourVampire.getScoresPhysical();
	for(int i = 0; i < scoresPhysical.size(); i++)
	{
		ourVampire.setExperience(scoresPhysical[i]*4);
	}
	
	// Social
	vector<int> scoresSocial = ourVampire.getScoresSocial();
	for(int i = 0; i < scoresSocial.size(); i++)
	{
		ourVampire.setExperience(scoresSocial[i]*4);
	}
	
	// Mental
	vector<int> scoresMental = ourVampire.getScoresMental();
	for(int i = 0; i < scoresMental.size(); i++)
	{
		ourVampire.setExperience(scoresMental[i]*4);
	}
	
	
	/// ABILITIES
	// retrieve final score for each adjectives (alertness, athletics etc...)
	vector<int> valueTALENT = ourVampire.getValueTalent();
	vector<int> valueSKILL = ourVampire.getValueSkills();
	vector<int> valueKNOWN = ourVampire.getValueKnowledges();
	for (int i = 0; i<6; i++) // not 11 because i have a problem with other spinbuttons (see function choiceAbilities)
	{
		if ( valueTALENT[i] != 0 ) // if score !=0 <=> new ability was selected
		{
			ourVampire.setExperience(3); //New Ability:3
			ourVampire.setExperience(valueTALENT[i]*2); //Ability:CRx2
		}
		if ( valueSKILL[i] != 0 )
		{
			ourVampire.setExperience(3); //New Ability:3
			ourVampire.setExperience(valueTALENT[i]*2); //Ability:CRx2
		}
		if ( valueKNOWN[i] != 0 )
		{
			ourVampire.setExperience(3); //New Ability:3
			ourVampire.setExperience(valueTALENT[i]*2); //Ability:CRx2
		}
	}
	
	
	/// New Discipline:10
	vector<string> disciplinesFinal = ourVampire.getDiscipline(ourVampire.GetClanName()); // retrieve disciplines selected
	ourVampire.setExperience(disciplinesFinal.size()*10); // how many new displinines we have
	
	for(int i = 0; i<6; i++){ 
		
		int valueScore = ourVampire.GetValueDisciplines(i); // retrieve score of each discipline
		ourVampire.setExperience(int(valueScore*5)); // Clan Discipline:CRx5
	} 		

}






/********************************************** AFTER SUBMITTING "Create vampire" button : CALCULATE EXPERIENCE AND OPEN OUTPUT WINDOW **********************************************************/
 void FirstInterface::openNewWindow()
 /** This function open the seconde interface: the paper with description of vampire, displaying all his scores
  * and resets scores of experience
 * @return NOTHING */
 {
	// Reset value of experience to 0, for each submitting "Create a vampire"
	ourVampire.resetExperience();
	// Calculate experience only after submit final vampire
	calculateExperience();
	
	// Retrieve name of player
	string playerNameGiven = textBufferPlayerName->get_text();
	ourVampire.setNamePlayer(playerNameGiven); // Save Payer Name
	
	Gtk::Window *secondInterface = NULL; // pointer on Gtk::Window()
	secondInterface = new SecondInterface(ourVampire); // dynamic definition
}
