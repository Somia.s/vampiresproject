#include "vampiresProject.h"
#include <vector>
#include <iostream>


/********************************************** DRAWING  **********************************************************/
Drawing::Drawing()
/** This function load image of final Page
 * and start to parameter the draw
 * @return NOTHING */	
{
	set_size_request(2000, 1000);
	// Load the image
	image = Gdk::Pixbuf::create_from_file("finalPage.png");
	add_events(Gdk::BUTTON_PRESS_MASK | Gdk::SCROLL_MASK  |Gdk::SMOOTH_SCROLL_MASK);
    scale=1;

}
Drawing::~Drawing() {}


bool Drawing::on_scroll_event(GdkEventScroll *ev)
/** This function add possibility to zoom on image
* @param ev: event of scrolling
 * @return true */	
{
    // Update scale according to mouse scroll
    scale-=ev->delta_y/10.;
	if (scale<0.1) scale=0.1;
    // Update drawing
    queue_draw();
    // There is probably a good reason to do this
    return true;
}


void Drawing::setText(string nameP, string nameN, string nameC, string nicknameClan, vector<string> disciplinesClan)
/** This function add in Drawing object, the strings of text we want to display on final page (the image)
 * @param nameP : text of player name
 * @param nameN : text of nature
 * @param nicknameClan : text of nickname clan
 * @param disciplinesClan : all texts of clan
 * @return NOTHING */
{
	namePlayer = nameP;
	nature = nameN;
	nameClan = nameC;
	nickname = nicknameClan;
	disciplines = disciplinesClan;
}


void Drawing::setCoordAttributes(vector<int> valuesPhysical, vector<int> valuesSocial, vector<int> valuesMental) // size of each vector = 3
/** This function set coordinates for Attributes
* @param valuesPhysical: vector with 3 scores of Physical
* @param valuesSocial: vector with 3 scores of Social
* @param valuesMental: vector with 3 scores of Mental
 * @return NOTHING */
{
	coordAttributes.clear(); // reset vector
	// According to value given, the coordinates (x,y) of point are different
	// RETRIEVE COORDINATES POINT OF SCORE GIVEN BY PLAYER
	for(int k=0; k<3; k++)
	{
		// retrieve scores of all attributes
		// PHYSICAL => coordinate x = 410, but coordinate y changes value (24 = length between Strength and Dexterity etc...)
		int valueP = valuesPhysical[k]; 
		coordAttributes.push_back(410 + 24*valueP); // for strength charisma and perception
		// SOCIAL
		int valueS = valuesSocial[k]; 
		coordAttributes.push_back(410 + 485 + 24*valueS); // skip a line
		// MENTAL
		int valueM = valuesMental[k]; 
		coordAttributes.push_back(410 + 485*2 + 24*valueM); // skip an other line
	}
}	

void Drawing::setCoordAbilities(vector<int> valuesTalents, vector<int> valuesSkills, vector<int> valuesKnowledges) // size of each vector = 3
/** This function set coordinates for Attributes
* @param valuesTalents: vector with 6 scores of Talents
* @param valuesSkills: vector with 6 scores of Skills
* @param valuesKnowledges: vector with 6 scores of Knowledges
 * @return NOTHING */
{
	coordAbilities.clear(); // reset vector
	// According to value given, the coordinates (x,y) of point are different
	// RETRIEVE COORDINATES POINT OF SCORE GIVEN BY PLAYER
	for(int k=0; k<6; k++)
	{
		// retrieve scores of all attributes
		// TALENTS
		int valueT = valuesTalents[k];
		coordAbilities.push_back(410 + 24*valueT); // for strength charisma and perception
		// SKILLS
		int valueS = valuesSkills[k]; 
		coordAbilities.push_back(410 + 485 + 24*valueS); // skip a line
		// KNOWLEDGES
		int valueK = valuesKnowledges[k]; 
		coordAbilities.push_back(410 + 485*2 + 24*valueK); // skip an other line

	}
}	


void Drawing::setCoordDisciplines(int index, int value)
/** Save coordinates x of each disciplines, because this is the value which changes with number of points 
 Indeed, coordinate y not change 
 * @param index: index of table containing disciplines, index fits with number of points given by player
 * @param value: coordinate x of disciplines
 * @return NOTHING */
{
	coordDisciplines[index] = 410 + 24*value; // fill table
}


void Drawing::setExperience(int valueExperience)
{
	experience = valueExperience;
}


int Drawing::getExperience()
{
	return experience;
}


/********************************************** FINAL INTERFACE  **********************************************************/
SecondInterface::SecondInterface(Personnage& ourVampire) // Constructor of the Second Interface
/** Paraleters the second interface 
 * Retrieve values from vampire 
 * @param ourVampire: object vampire created before during interface one
 * @return NOTHING */
{
	// Initialize the main window
    this->set_title("--VAMPIRES-- Second Interface");
    this->set_border_width(10);
    this->set_position(Gtk::WIN_POS_CENTER);
	this->resize(2000, 1000); 
	
	
	////////////////////////////////////////////////////////////
	//////// Retrieve values given by player currently /////////
	//////////////// From ourVampire object ////////////////////
	////////////////////////////////////////////////////////////
	
	// Player Name
	string playerNameGiven = ourVampire.getNamePlayer();
	
	// Nature and Demeanor
	string natureGiven = ourVampire.GetNature_and_Demeanor();
	
	// Clan Name
	string nameClanGiven = ourVampire.GetClanName();		
	
	// Nickname
	string nicknameClan = ourVampire.getNickname(nameClanGiven);
	
	// disciplines
	vector<string> disciplinesClan = ourVampire.getDiscipline(nameClanGiven);
	
	// Insert into drawing object named Dwg
	Dwg.setText(playerNameGiven, natureGiven, nameClanGiven, nicknameClan, disciplinesClan);
	
	////  ATTRIBUTES
	// Scores for Physical
	vector<int> valuesPhysical = ourVampire.getScoresPhysical();
	// Scores for Social
	vector<int> valuesSocial = ourVampire.getScoresSocial();
	// Scores for Mental
	vector<int> valuesMental = ourVampire.getScoresMental();
	
	// recording all vectors in Dwg object
	 Dwg.setCoordAttributes(valuesPhysical, valuesSocial, valuesMental);

	////  ABILITIES						
	vector<int> vectorTal = ourVampire.getValueTalent();
	vector<int> vectorSk = ourVampire.getValueSkills();
	vector<int> vectorKnown = ourVampire.getValueKnowledges();	
	Dwg.setCoordAbilities(vectorTal, vectorSk, vectorKnown); // Save values in Dwg object
	
	////  DISCIPLINES
	for (int i=0; i<6; i++)
	{
		int value = ourVampire.GetValueDisciplines(i);
		Dwg.setCoordDisciplines(i, value);
	}
	
	// Experiences
	Dwg.setExperience(ourVampire.getExperience());

	///////////////////////////////////////////////////////////////
	//////// END Retrieve values given by player currently ////////
	//////////////// END From ourVampire object ///////////////////
	///////////////////////////////////////////////////////////////
	

	m_ScrolledWindow.add(Dwg); // include drawing in scroll
	this->add(m_ScrolledWindow); // scroll in window
	this->show_all_children();
	this->show(); // show window

}

// Destructor of the class
SecondInterface::~SecondInterface() {}






/********************************************** WRITE TEXT **********************************************************/
void Drawing::draw_text(const Cairo::RefPtr<Cairo::Context>& cr, int x, int y, string text)
{
	cr->save();
	
	Pango::FontDescription font;
	// Layout
	font.set_family("Courier");
	font.set_size(20 * PANGO_SCALE);
	font.set_style(Pango::STYLE_ITALIC);
	font.set_weight(Pango::WEIGHT_ULTRABOLD);

	// Add text
	auto layout = create_pango_layout(text);
	layout->set_font_description(font);

	// Color: red
	cr->set_source_rgba(0.8, 0.0, 0.0, 0.7);

	// Position according coordinates x and y
	cr->move_to(x, y);
	
	// Show text
	layout->show_in_cairo_context(cr);
	
	cr->stroke();
	cr->restore();
}




/********************************************** DRAW POINT **********************************************************/
void Drawing::createOnePoint(const Cairo::RefPtr<Cairo::Context>& cr, int x, int y)
{
	Gtk::Allocation allocation = get_allocation();
	const int width = allocation.get_width();
	
	// 1 POINT
	cr->save();
	cr->set_source_rgba(0.8, 0.0, 0.0, 0.7); // red
	cr->set_line_width(1); // width of line
	//cr->arc(image->get_width()/20 ,image->get_height()/20 ,width/100 ,0 , 2*M_PI); // width = 1776 and height = 2276
		
	cr->arc(x, y, width/100 , 0 , 2*M_PI); // first argument = coordinate x, second argument = coordinate y, third argument = size of point

	cr->fill_preserve(); // fill the point
	cr->stroke();
	cr->restore();
}




/********************************************** DRAW  **********************************************************/
bool Drawing::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
	// Get drawing area size
	Gtk::Allocation allocation = get_allocation();
	const int width = allocation.get_width();
	const int height = allocation.get_height();

	// Scale the image to the area
	//cr->scale((double)width/image->get_width(),(double)height/image->get_height());
	cr->scale(scale,scale);

	// IMAGE
	cr->save();
	// Place the image at 0,0
	Gdk::Cairo::set_source_pixbuf(cr, image, 0,0);
	// Update the area where the image is located
	cr->rectangle(0, 0, image->get_width(), image->get_height());
	// Fill the area with the image
	cr->fill();
	cr->restore();
	

	////////////////////////// WRITE TEXT ///////////////////////////

	// Player Name
	draw_text(cr, 286, 347, namePlayer);
	// Nature and Demeanor
	draw_text(cr, 780, 300, nature);
	draw_text(cr, 825, 347, nature);
	// CLAN
	draw_text(cr, 1220, 300, nameClan);
	// Nickname
	draw_text(cr, 286, 300, nickname);
	// Displines
	int i = 0;
	for (vector<string>::iterator it=disciplines.begin(); it!=disciplines.end(); ++it)
	{
		draw_text(cr, 160, 1258+i, *it);
		i=i+37;
	}
	// Experience
	draw_text(cr, 2060, 1330, to_string(experience));
    
	////////////////////////// DRAW POINTS ///////////////////////////
	
	///////////////////////////////////////
	///////////// ATTRIBUTES //////////////
	///////////////////////////////////////
			
	int coordY = 549;
	int l = 0;
	for (int index=0; index<9; index++)
	{
		// Strength Charisma Perception SAME LINE
		// Dexterity Manipulation Intelligence SAME LINE and need to skip a line
		// Stamina Appearance Wits SAME LINE and need to skip a other line
				
		createOnePoint(cr, coordAttributes[index], coordY); // just x value changes
		l+=1;
		if (l==3 || l==6) // change line after 3 points
			{coordY+= 36;} // distance between two lines
	}
	
	//////////////////////////////////
	////////// ABILITIES /////////////
	//////////////////////////////////
	
	int coordinateY = 763;
	int line = 0;
	for (int index=0; index<18; index++)
	{
		// Strength Charisma Perception SAME LINE
		// Dexterity Manipulation Intelligence SAME LINE and need to skip a line
		// Stamina Appearance Wits SAME LINE and need to skip a other line
				
		createOnePoint(cr, coordAbilities[index], coordinateY); // just x value changes
		line+=1;
		if (line==3 || line==6 || line==9 || line==12|| line==15) // change line after 3 points
			{coordinateY+= 37;} // distance between two lines
	}
	
	//////////////////////////////////
	////////// ADVANTAGES ////////////
	//////////////////////////////////
	
	// Displines
	int j =0;
	for (int i=0; i<6; i++)
	{
		int coordinate = coordDisciplines[i];
		createOnePoint(cr, coordinate, 1265+j);
		j+=37; // skip a line
	}
	
	return true;
}
