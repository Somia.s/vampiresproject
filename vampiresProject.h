#ifndef GTKMM_VAMPIRESPROJECT_H
#define GTKMM_VAMPIRESPROJECT_H

#include <iostream>
#include <string>
#include <vector>
#include <gtkmm.h>
#include <gtkmm/application.h>
#include <set>
#include <fstream>
#include <map>
using namespace std;






/********************************************************************************************************/
class DistributionPoints
{
	protected:
		int nbPointsFree;
		int nbPointsExperience;
		int nbAttributes;
		int nbAbility;
		int nbDisciplines;
		
	public:
		void InitAdvantages();
		
		// Experiences		
		void setExperience(int);
		int getExperience();
		void resetExperience();
		
		// Free points
		void changeFreePoints(int, string);
		int setFreePoints();
		int getAttributesFree();
		int getAbilityFree();
		int getDisciplinesFree();
		
};


/********************************************************************************************************/
class OtherTraits
{
	protected:
		
		int nbPointsBackground;
		vector <string> backgroundName;
		vector <int> backgroundPoints;
		
		int nbPointsVirtues;
		int conscience;
		int selfControl;
		int courage;
		
		int humanity; 	// (equal to Conscience + Self-Control), 
		int willpower; 	//(equal to Courage)
		int bloodPool;
		
		int nbPointsMeritsAndFlawsMAX;
		int nbPointsMeritsAndFlaws;
		vector <string> meritsAndFlaws;
		vector <int> valueMeritsAndFlaws;

	public:
		void InitTraits();
		void SetBackground(string, int);
		void setVirtuesPoints(string, int);
		int getVirtuesPoints();
		int getConsience();
		int getSelfControl();
		int getCourage();
		void setHumanity();
		void setWillPower();
};





/********************************************************************************************************/
class Abilities
{
	protected:
		int talents;
		int skills;
		int knowledges;
		int maxInitAbilities;
		
		vector<int> vectorTalent;	
		vector<int> vectorSkills;
		vector<int> vectorKnowledges;	

		
	public:
		void InitialiseAbilities();
		
		void setTalentPoints(int);
		void setSkillsPoints(int);
		void setKnowledgesPoints(int);
		
		int getTalentPoints();
		int getSkillsPoints();
		int getKnowledgesPoints();

		void setTalent(int);
		vector<int> getValueTalent();
		void resetTalent();
		void setSkills(int);
		vector<int> getValueSkills();
		void resetSkills();
		void setKnowledges(int);
		vector<int> getValueKnowledges();
		void resetKnowledges();
};



/********************************************************************************************************/
class Attributes
{
	protected:
		int physical, social, mental;
		
		int strength, dexterity, stamina;
		int charisma, manipulation, appearance;
		int perception, intelligence, wits;
		
		vector<int> scoresPhysical; // vector containing score of strength, dexterity and stamina
		vector<int> scoresSocial;
		vector<int> scoresMental;
	
		
	public:
		void InitialiseAttribute(string);
		
		// Set value given by player
		void setPhysicalPoints(int);
		void setSocialPoints(int);
		void setMentalPoints(int);
		
		// Retrieve value for distribution inside Physical, Social and Mental
		int getPhysicalPoints();
		int getSocialPoints();
		int getMentalPoints();
		
		// Retrieve value chosen by player for each attribute
		// PHYSICAL
		void distribPhysical(int, int, int);
		vector<int> getScoresPhysical();
		// SOCIAL
		void distribSocial(int, int, int);		
		vector<int> getScoresSocial();	
		// MENTAL
		void distribMental(int, int, int);	// attention a l'apparence add (string)
		vector<int> getScoresMental();
};




/********************************************************************************************************/
/* Cette classe memorize et manipule les donnees sur les clans de vampire */
class Clan
{
	protected:
		string clanName;						/// Nom du clan de votre vampire
		
		string nickname;						/// Surnom du clan
		string background;						/// Passe commun des membres de votre clan
		string sect;							/// Secte a laquelle les vampires de votre clan appartient habituellement	
		string haven;							///
		string appearance;						///
		string character;
		int nbPointsDisciplines;					/// Nombre de points a distribuer dans les disciples
		vector <string> disciplines;					/// Tableau de nom des disciplines choisies
		int valueDisciples[6];					/// Tableau des points investits dans la disciplines choisi
		string weakness; 
		string organization;
		
		string nature_and_demeanor;
		
	public:
		string GetClanName();
		void SetClanName(string);
		string GetNickname();
		void SetNickname(string);
		string GetBackground();
		void SetBackground(string);
		string GetSect();
		void SetSect(string);
		string GetHaven();
		void SetHaven(string);
		string GetAppearance();
		void SetAppearance(string);
		string GetCharacter();
		void SetCharacter(string);
		int GetValueDisciplines(int);
		void setDisciplines(int, int);
		string GetWeakness();
		void SetWeakness(string);
		string GetOrganization();
		void SetOrganization(string);
		
		string GetNature_and_Demeanor();
		void SetNature_and_Demeanor(string);
		
};


/********************************************************************************************************/
class File
{
	protected:
		set<string> allNatures;
		set<string> allClans;
		map<string, string> allNicknames;
		map<string, string> descriptionsClans;
		map<string, string> descriptionsNatures;
		map<string, vector<string>> allDisciplines;
		vector<string> tokenize(const string& s, string delim); // parse a string with a delimiter (only on character)
		
	public:
		// PARSE FILE
		void readFileNatures();
		void readFileClans();

		// GET INFORMATIONS FROM PARSING
		set<string> getNatures();
		set<string> getClans();
		string getNickname(string); // parameter = name of clan
		string getDescriptionClan(string);  // parameter = name of clan
		string getDescriptionNature(string);  // parameter = name of clan
		vector<string> getDiscipline(string); // parameter = name of clans
		
};


/********************************************************************************************************/
class Personnage : public Clan, public Attributes, public Abilities, public OtherTraits, public DistributionPoints, public File
{
	protected:
		// About player
		string namePlayer;
		set<string> allClans;
		
	public:
		void setNamePlayer(string);
		string getNamePlayer();
};



/********************************************************************************************************/
class FirstInterface: public Gtk::Window // The class FirstInterface inherits from Gtk::Window
{
	protected:
		
		
		/////////////////////////////////
		//////// Create personnage  /////
		/// An interface = 1 vampire ////
		/////////////////////////////////
		
		//Personnage ourVampire, creation of object
		Personnage ourVampire;
		
		// Some informations (text)
		string clanChosen;
		string natureChosen;
		   
		//Member widgets:
		
		/////////////////////////////////
		////////// PLAYER NAME //////////
		/////////////////////////////////
		
		// Player writes his/her name
		Gtk::ScrolledWindow scrollPlayerName;
		Gtk::TextView TViewPlayerName;
		Glib::RefPtr<Gtk::TextBuffer> textBufferPlayerName;
		void fillBuffersPlayerName();
		void on_button_bufferPlayerName();
		Gtk::Box boxPlayerName;
		Gtk::Frame framePlayerName;
		
		/////////////////////////////////
		/////////// NATURE //////////////
		/////////////////////////////////
		
		// To choice nature
		Gtk::Frame frameNature;
		Gtk::Box boxNature;
		Gtk::Label labelNature;
		Gtk::ComboBoxText comboBoxNature; 
		Gtk::Label textChoiceNature;
		Gtk::Button buttonNature;
		
		/////////////////////////////////
		///////////// CLAN //////////////
		/////////////////////////////////	
		
		// To choice the clan
		Gtk::Frame frameClan;
		Gtk::Box boxClan;
		Gtk::Label labelCLAN;
		Gtk::ComboBoxText comboBoxCLAN; 
		Gtk::Label textChoiceCLAN;
		Gtk::Button buttonCLAN;
		
		/////////////////////////////////
		//////// DESCRIPTION CLAN ///////
		/////////////////////////////////
		
		// Display description of clan chosen
		Gtk::ScrolledWindow scrollDescription;
		Gtk::TextView TViewDescription;
		Glib::RefPtr<Gtk::TextBuffer> textBufferDescription;
		void fillBuffersDescription(string);
		void on_button_bufferDescription();
		Gtk::Box boxDescription;
		Gtk::Frame frameDescription;
		
		
		/////////////////////////////////
		//////// DESCRIPTION NATURE ///////
		/////////////////////////////////
		
		// Display description of clan chosen
		Gtk::ScrolledWindow scrollDescriptionN;
		Gtk::TextView TViewDescriptionN;
		Glib::RefPtr<Gtk::TextBuffer> textBufferDescriptionN;
		void fillBuffersDescriptionN(string);
		void on_button_bufferDescriptionN();
		Gtk::Box boxDescriptionN;
		Gtk::Frame frameDescriptionN;

			
		///////////////////////////////////////
		///////////// FREEBIE POINTS //////////
		///////////////////////////////////////
		
		Gtk::Frame frameFreebie;
		Gtk::Box boxFreebie;
		Gtk::Label labelFreebie;
		Gtk::ComboBoxText comboBoxFreebie;
		Gtk::Label textChoiceFreebie;
		Gtk::Button buttonFreebie;
		Gtk::Button buttonResetFreebie;
		
		
		
		///////////////////////////////////////
		///////////// ATTRIBUTES //////////////
		///////////////////////////////////////
		
		Gtk::ComboBoxText ComboBoxAttributes; // How distribute points between Physical Social and Mental ?
		Gtk::Box boxChoiceAttributes;
		Gtk::Button submitAttributes;
		Gtk::Frame frameAttributes;
		Gtk::Label labelAttributes;
		
		/// PHYSICAL : One button for Physical
		Gtk::Button buttonPhysical;
		// STRENGTH
		Gtk::SpinButton spinStrength;
		Gtk::Label textChoiceStrength;
		Gtk::Box boxStrength;
		Gtk::Frame frameStrength;
		// DEXTERITY
		Gtk::SpinButton spinDexterity;
		Gtk::Label textChoiceDexterity;
		Gtk::Box boxDexterity;
		Gtk::Frame frameDexterity;
		// STAMINA
		Gtk::SpinButton spinStamina;
		Gtk::Label textChoiceStamina;
		Gtk::Box boxStamina;
		Gtk::Frame frameStamina;
		/// END PHYSICAL
		
		/// SOCIAL
		Gtk::Button buttonSocial;
		// Charisma
		Gtk::SpinButton spinCharisma;
		Gtk::Label textChoiceCharisma;
		Gtk::Box boxCharisma;
		Gtk::Frame frameCharisma;
		// Manipulation
		Gtk::SpinButton spinManipulation;
		Gtk::Label textChoiceManipulation;
		Gtk::Box boxManipulation;
		Gtk::Frame frameManipulation;
		// Appearance
		Gtk::SpinButton spinAppearance;
		Gtk::Label textChoiceAppearance;
		Gtk::Box boxAppearance;
		Gtk::Frame frameAppearance;
		/// END SOCIAL
		
		/// MENTAL
		Gtk::Button buttonMental;		
		// Perception
		Gtk::SpinButton spinPerception;
		Gtk::Label textChoicePerception;
		Gtk::Box boxPerception;
		Gtk::Frame framePerception;
		// Intelligence
		Gtk::SpinButton spinIntelligence;
		Gtk::Label textChoiceIntelligence;
		Gtk::Box boxIntelligence;
		Gtk::Frame frameIntelligence;
		// Wits
		Gtk::SpinButton spinWits;
		Gtk::Label textChoiceWits;
		Gtk::Box boxWits;
		Gtk::Frame frameWits;
		/// END MENTAL
		
		///////////////////////////////////////
		///////////// END ATTRIBUTES //////////
		///////////////////////////////////////
		
		
		//////////////////////////////////////
		///////////// ABILITIES //////////////
		//////////////////////////////////////
		
		Gtk::ComboBoxText ComboBoxAbilities; // How distribute points between Talents, Skills and Knowledges ?
		Gtk::Box boxChoiceAbilities;
		Gtk::Button submitAbilities;
		Gtk::Frame frameAbilities;
		Gtk::Label labelAbilities;
		
		/// TALENTS
		Gtk::Button buttonTalents;
		// Alertness
		Gtk::SpinButton spinAlertness; Gtk::Label textChoiceAlertness; Gtk::Box boxAlertness; Gtk::Frame frameAlertness;
		// Athletics
		Gtk::SpinButton spinAthletics; Gtk::Label textChoiceAthletics; Gtk::Box boxAthletics; Gtk::Frame frameAthletics;
		// Awareness
		Gtk::SpinButton spinAwareness; Gtk::Label textChoiceAwareness; Gtk::Box boxAwareness; Gtk::Frame frameAwareness;
		// Brawl
		Gtk::SpinButton spinBrawl; Gtk::Label textChoiceBrawl; Gtk::Box boxBrawl; Gtk::Frame frameBrawl;
		// Empathy
		Gtk::SpinButton spinEmpathy; Gtk::Label textChoiceEmpathy; Gtk::Box boxEmpathy; Gtk::Frame frameEmpathy;
		// Expression
		Gtk::SpinButton spinExpression; Gtk::Label textChoiceExpression; Gtk::Box boxExpression; Gtk::Frame frameExpression;
		// Intimidation
		Gtk::SpinButton spinIntimidation; Gtk::Label textChoiceIntimidation; Gtk::Box boxIntimidation; Gtk::Frame frameIntimidation;
		// Leadership
		Gtk::SpinButton spinLeadership; Gtk::Label textChoiceLeadership; Gtk::Box boxLeadership; Gtk::Frame frameLeadership;
		// Streetwise
		Gtk::SpinButton spinStreetwise; Gtk::Label textChoiceStreetwise; Gtk::Box boxStreetwise; Gtk::Frame frameStreetwise;
		// Subterfuge
		Gtk::SpinButton spinSubterfuge; Gtk::Label textChoiceSubterfuge; Gtk::Box boxSubterfuge; Gtk::Frame frameSubterfuge;
		
		/// SKILLS
		Gtk::Button buttonSkills;
		// AnimalKen
		Gtk::SpinButton spinAnimalKen; Gtk::Label textChoiceAnimalKen; Gtk::Box boxAnimalKen; Gtk::Frame frameAnimalKen;
		// Crafts
		Gtk::SpinButton spinCrafts; Gtk::Label textChoiceCrafts; Gtk::Box boxCrafts; Gtk::Frame frameCrafts;
		// Drive
		Gtk::SpinButton spinDrive; Gtk::Label textChoiceDrive; Gtk::Box boxDrive; Gtk::Frame frameDrive;
		// Etiquette
		Gtk::SpinButton spinEtiquette; Gtk::Label textChoiceEtiquette; Gtk::Box boxEtiquette; Gtk::Frame frameEtiquette;
		// Firearms
		Gtk::SpinButton spinFirearms; Gtk::Label textChoiceFirearms; Gtk::Box boxFirearms; Gtk::Frame frameFirearms;
		// Larceny
		Gtk::SpinButton spinLarceny; Gtk::Label textChoiceLarceny; Gtk::Box boxLarceny; Gtk::Frame frameLarceny;
		// Melee
		Gtk::SpinButton spinMelee; Gtk::Label textChoiceMelee; Gtk::Box boxMelee; Gtk::Frame frameMelee;
		// Performance
		Gtk::SpinButton spinPerformance; Gtk::Label textChoicePerformance; Gtk::Box boxPerformance; Gtk::Frame framePerformance;
		// Stealth
		Gtk::SpinButton spinStealth; Gtk::Label textChoiceStealth; Gtk::Box boxStealth; Gtk::Frame frameStealth;
		// Survival
		Gtk::SpinButton spinSurvival; Gtk::Label textChoiceSurvival; Gtk::Box boxSurvival; Gtk::Frame frameSurvival;
		
		/// KNOWLEDGES		
		Gtk::Button buttonKnowledges;
		// Academics
		Gtk::SpinButton spinAcademics; Gtk::Label textChoiceAcademics; Gtk::Box boxAcademics; Gtk::Frame frameAcademics;
		// Computer
		Gtk::SpinButton spinComputer; Gtk::Label textChoiceComputer; Gtk::Box boxComputer; Gtk::Frame frameComputer;
		// Finance
		Gtk::SpinButton spinFinance; Gtk::Label textChoiceFinance; Gtk::Box boxFinance; Gtk::Frame frameFinance;
		// Investigation
		Gtk::SpinButton spinInvestigation; Gtk::Label textChoiceInvestigation; Gtk::Box boxInvestigation; Gtk::Frame frameInvestigation;
		// Law
		Gtk::SpinButton spinLaw; Gtk::Label textChoiceLaw; Gtk::Box boxLaw; Gtk::Frame frameLaw;
		// Medecine
		Gtk::SpinButton spinMedecine; Gtk::Label textChoiceMedecine; Gtk::Box boxMedecine; Gtk::Frame frameMedecine;
		// Occult
		Gtk::SpinButton spinOccult; Gtk::Label textChoiceOccult; Gtk::Box boxOccult; Gtk::Frame frameOccult;
		// Politics
		Gtk::SpinButton spinPolitics; Gtk::Label textChoicePolitics; Gtk::Box boxPolitics; Gtk::Frame framePolitics;
		// Science
		Gtk::SpinButton spinScience; Gtk::Label textChoiceScience; Gtk::Box boxScience; Gtk::Frame frameScience;
		// Technology
		Gtk::SpinButton spinTechnology; Gtk::Label textChoiceTechnology; Gtk::Box boxTechnology; Gtk::Frame frameTechnology;
		
		
		//////////////////////////////////////
		///////////// END ABILITIES //////////
		//////////////////////////////////////
		
		
		//////////////////////////////////////
		////////////// DISCIPLINES ///////////
		//////////////////////////////////////
		
		// For submit
		Gtk::Button submitDisciplines;
		
		Gtk::SpinButton spinD1;
		Gtk::Label textChoiceD1; 
		Gtk::Box boxD1; 
		Gtk::Frame frameD1;
		
		Gtk::SpinButton spinD2;
		Gtk::Label textChoiceD2; 
		Gtk::Box boxD2; 
		Gtk::Frame frameD2;
		
		Gtk::SpinButton spinD3;
		Gtk::Label textChoiceD3; 
		Gtk::Box boxD3; 
		Gtk::Frame frameD3;
		
		Gtk::SpinButton spinD4;
		Gtk::Label textChoiceD4; 
		Gtk::Box boxD4; 
		Gtk::Frame frameD4;
		
		Gtk::SpinButton spinD5;
		Gtk::Label textChoiceD5; 
		Gtk::Box boxD5; 
		Gtk::Frame frameD5;
		
		Gtk::SpinButton spinD6;
		Gtk::Label textChoiceD6; 
		Gtk::Box boxD6; 
		Gtk::Frame frameD6;
		
		//////////////////////////////////////
		///////////// END DISCIPLINES ////////
		//////////////////////////////////////
		
		
		/// FUNCTIONS
		
		// Create composants
		void createSpinButton(Gtk::SpinButton&, Gtk::Box&, Gtk::Frame&, string , Gtk::Grid&, int, int, Gtk::Label&);
		void createComboBOX(string, set<string>, Gtk::ComboBoxText&, Gtk::Label&, Gtk::Box&, Gtk::Button&, string, Gtk::Frame&, string, Gtk::Grid&, int);

		//Signal handlers (run when the button are clicked), execute composants
		void choiceNature();
		void choiceCLAN();
		void distributeAttributes(); // Give 7 points or 5 or 3 for physical ...
		void distributeAbilities();
		void choiceAttributes(string, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::Label&, Gtk::Label&, Gtk::Label&); // display and retrieve values given by player
		void choiceAbilities(string, Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&,  Gtk::SpinButton&, Gtk::SpinButton&, Gtk::SpinButton&); // display and retrieve values given by player
		void choiceDisciplines();
		void distributeFreebies();
		void resetFreebies();
		void calculateExperience();
		
		// End of interface
		Gtk::ScrolledWindow SCROLL;
		Gtk::Button buttonQuit;
		Gtk::Grid mainGrid;
	
	
	public:
		FirstInterface();
		virtual ~FirstInterface(); // Constructor and destructor
		
		// To retrieve value choiced by player
		void openNewWindow();
};



/********************************************************************************************************/
class Drawing : public Gtk::DrawingArea
{		
	private:
		// Scale of the image
		double scale;
		void draw_text(const Cairo::RefPtr<Cairo::Context>& cr, int, int, string);
				
	protected:
		//Override default signal handler:
		virtual bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);
		Glib::RefPtr<Gdk::Pixbuf> image;
		
		// Override mouse scroll event
		bool on_scroll_event(GdkEventScroll *ev);
	
		void createOnePoint(const Cairo::RefPtr<Cairo::Context>& cr, int x, int y); // creation of 1 red point
		
		/// INFORMATIONS TO DISPLAY ON IMAGE //
		// Clan, Payer name
		string nameClan; string namePlayer;
		// Nature and Demeanor
		string nature;
		// Nickname of clan
		string nickname;
		// Displines of clan
		vector<string> disciplines;
		/// SAVE COORDINATES //
		// ATTRIBUTES
		vector<int> coordAttributes;
		// ABILITIES
		vector<int> coordAbilities;
		// Disciplines
		int coordDisciplines[6];
		// Experiences
		int experience;
		
	public:
		Drawing(); // passer en argument l'objet ourVampire 
		virtual ~Drawing();
		
		// INFORMATIONS TO DISPLAY ON IMAGE
		void setText(string, string, string, string, vector<string>);
		
		void setCoordAttributes(vector<int>, vector<int>, vector<int>);
		
		void setCoordAbilities(vector<int>, vector<int>, vector<int>);
		
		void setCoordDisciplines(int, int);
		void setExperience(int);
		int getExperience();
};




/********************************************************************************************************/
class SecondInterface: public Gtk::Window // The class FirstInterface inherits from Gtk::Window
{
	protected:
		Drawing Dwg; // Appel de dessin dans la classe fenêtre
		Gtk::ScrolledWindow m_ScrolledWindow;
		Gtk::Grid mainGrid2;
		
	public:
		SecondInterface(Personnage&); // parameter  = our object ourVampire
		virtual ~SecondInterface(); // Constructor and destructor
		
};


#endif //GTKMM_VAMPIRESPROJECT_H

