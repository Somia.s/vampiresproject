### Auteure

Somia SAIDI

### Goal
Create a vampire filling points

### Steps

## Step 1: Install g++ and gtkmm

```
sudo apt install g++
apt-get install libgtkmm-3.0-dev
```

## Step 2: Compile files and execute the first interface

```
g++ vampiresMain.cpp -o FirstWindow `pkg-config gtkmm-3.0 --cflags --libs`
```

Then

```
./FirstWindow
```

## Step 2: Give your name

## Step 3: Choose a nature and a clan

After submitting your choice, informations about the nature or the clan appear. You can change your mind!

Nickname of clan appear in final interface

## Step 4: Distribute points freebies

Select additionnal points

> RULES:
> 
> Attribute: 5 per dot
> 
> Ability: 2 per dot
> 
> Discipline: 7 per dot
> 
> Background: 1 per dot
> 
> Virtue: 2 per dot
> 
> Humanity: 2 per dot
> 
> Willpower: 1 per dot



15 points only can be given, you can give 5*3 attributes for example.

## Step 5: Distribute points between Physical, Social, and Mental

Give 13 points or 9 or 5 for each , you can't give same points for each

Freebie points are also added. If you submit much values according other submitting, interface displays you that's not correct.

**Example:** You can give only 8 points for each adjective.
If you give 5 points of freebies points for Attributes. And you have given 2 freebies points for Social, you can give only 3 points for Physical and Mental.


## Step 6: Give points for Attributes

Warning: We can give points only after submitting clan

If you have chosen "Nosferatu" clan, appearance is equal to 0.

## Step 7: Distribute points between Talents, Skills, ans Knowledges

Like step 5

## Step 8: Disciplines

Disciplines appears only after submitting a clan.
Distribute 3 points (or more according freebie points)

## Step 9: Experiences

More you play and more you won experience points. Result appears in final interface

> RULES:
> 
> New Ability:3
> 
> New Discipline:10
> 
> Attribute:CRx4
> 
> Ability:CRx2
> 
> Clan Discipline:CRx5
> 
> CR is the current rating of the trait



**Example1:**
You have chosen only a clan. So you win 1 point given automatically for each attribute x 4 (total=9 attributesx4=36).
Thanks to this clan, you have automatically 3 disciplines, so you win 3*10 (because new discipline) points.

Total = 36+30 = 66

**Example2:** if you give: 
- 1 point for a talent (alertness)
- 2 points for one discipline 
- and you clan chosen have 3 disciplines.

So, you win: 
1 point given automatically for each attributes * 4 (total=9 attributes*4=36)
+ 3 points for new ability (talent) 
+ 1 point*2 (2 CR discipline) 
+ 10*3 (new disciplines of this clan) 
+ 2 points*5 (because one point for each discipline give 5 points)

Total = 81 points.
	

## Step 10: Creation of your vampire

After submit last button of interface, a character page appear with your vampire!
