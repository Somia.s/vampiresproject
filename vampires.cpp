#include "vampiresProject.h"






/********************************************************************************************************/
void DistributionPoints::InitAdvantages()
{
	nbPointsFree = 15; 
	nbPointsExperience = 0;
	nbAttributes = 0;
	nbAbility = 0;
	nbDisciplines = 0;
}

void DistributionPoints::resetExperience()
{
	nbPointsExperience = 0;
}

void DistributionPoints::setExperience(int retirePoints)
{
	nbPointsExperience = nbPointsExperience + retirePoints;
}

int DistributionPoints::getExperience()
{
	return nbPointsExperience;
}


void DistributionPoints::changeFreePoints(int points, string choice)
{
	nbPointsFree = points; // new value, how points remain to distribute
	
	if (choice.compare("Attributes") == 0)
	{
		nbAttributes = nbAttributes + 5;
	}
	if (choice.compare("Ability") == 0)
	{
		nbAbility = nbAbility + 2;
	}
	if (choice.compare("Discipline") == 0)
	{
		nbDisciplines = nbDisciplines + 7;
	}
	//Attribute		5 per dot
	//Ability		2 per dot
	//Discipline		7 per dot
	//Background		1 per dot
	//Virtue		2 per dot
	//Humanity 		2 per dot
	//Willpower		1 per dot
}

int DistributionPoints::setFreePoints()
{
	return nbPointsFree;
}

int DistributionPoints::getAttributesFree()
{
	return nbAttributes;
}
int DistributionPoints::getAbilityFree()
{
	return nbAbility;
}
int DistributionPoints::getDisciplinesFree()
{
	return nbDisciplines;
}






















/********************************************************************************************************/
void OtherTraits::InitTraits()
{
	nbPointsBackground = 5;
	
	nbPointsVirtues = 7;
	conscience = 0;
	selfControl = 0;
	courage = 0;
	
	humanity = 0;
	willpower = 0;
	bloodPool = 10;
	
	nbPointsMeritsAndFlaws = 0;
	nbPointsMeritsAndFlawsMAX = 7;
}

void OtherTraits::SetBackground(string nomBack, int nbPoints)
{
	if(nbPointsBackground > 0 && nbPointsBackground - nbPoints >= 0)
	{
		backgroundName.push_back(nomBack);
		backgroundPoints.push_back(0);
		
		nbPointsBackground = nbPointsBackground - nbPoints;
	}
}

/*************************************************************************************************************************************************/
/** Cette fonction va d'ajouter des points dans les differentes vertues si c'est possible 
 * @param vertue : le nom de la virtue dans laquelle on veux rajouter des points
 * @param ajout : le nombre de points que l'on veut ajouter a la vertue 
 * @return RIEN */
void OtherTraits::setVirtuesPoints(string vertue, int ajout)
{
	if(nbPointsVirtues > 0 && nbPointsVirtues - ajout >= 0)
	{
		if(vertue.compare("conscience") == 0)
			conscience = conscience + ajout;
		if(vertue.compare("selfControl") == 0)
			selfControl = selfControl + ajout;
		if(vertue.compare("courage") == 0)
			courage = courage + ajout;
		
		nbPointsVirtues = nbPointsVirtues - ajout;
	}
}

int OtherTraits::getVirtuesPoints()
{
	return nbPointsVirtues;
}





/********************************************************************************************************/
void Abilities::InitialiseAbilities()
{
	maxInitAbilities = 3;
	talents = 0;
	skills = 0;
	knowledges = 0;

	for(int i=0; i<6; i++)
		vectorTalent.push_back(0);
	for(int i=0; i<6; i++)
		vectorSkills.push_back(0);
	for(int i=0; i<6; i++)
		vectorKnowledges.push_back(0);
}

void Abilities::setTalentPoints(int nbPoints)
{
	talents = nbPoints;
}

void Abilities::setSkillsPoints(int nbPoints)
{
	skills = nbPoints;
}

void Abilities::setKnowledgesPoints(int nbPoints)
{
	knowledges  = nbPoints;
}

int Abilities::getTalentPoints()
{
	return talents;
}

int Abilities::getSkillsPoints()
{
	return skills;
}

int Abilities::getKnowledgesPoints()
{
	return knowledges;
}

// SET
void Abilities::setTalent(int value)
{
	vectorTalent.push_back(value);
	
}
void Abilities::setSkills(int value)
{
	vectorSkills.push_back(value);

}

void Abilities::setKnowledges(int value)
{
	vectorKnowledges.push_back(value);
}

// GET

vector<int> Abilities::getValueTalent()
{
	return vectorTalent;
}


vector<int> Abilities::getValueSkills()
{
	return vectorSkills;
}


vector<int> Abilities::getValueKnowledges()
{
	return vectorKnowledges;
}

// RESET
void Abilities::resetTalent()
{
	vectorTalent.clear();
	
}
void Abilities::resetSkills()
{
	vectorSkills.clear();

}

void Abilities::resetKnowledges()
{
	vectorKnowledges.clear();
}





/********************************************************************************************************/
void Attributes::InitialiseAttribute(string clanName)
{
	physical = 0;
	social = 0; 
	mental = 0;
	
	strength = 1; 
	dexterity = 1; 
	stamina = 1; 
	
	charisma = 1;  
	manipulation = 1; 
	if( clanName == "Nosferatu" )
		appearance = 0; 
	else
		appearance = 1;
	
	perception = 1;  
	intelligence = 1; 
	wits = 1; 

}

// Set and get how points we want give for Physical Social and Mental

void Attributes::setPhysicalPoints(int nbPoints)
{
	physical = nbPoints;
}

void Attributes::setSocialPoints(int nbPoints)
{
	social = nbPoints;
}

void Attributes::setMentalPoints(int nbPoints)
{
	mental  = nbPoints;
}

int Attributes::getPhysicalPoints()
{
	return physical;
}

int Attributes::getSocialPoints()
{
	return social;
}

int Attributes::getMentalPoints()
{
	return mental;
}

// Distribute scores

void Attributes::distribPhysical(int strengthDone, int dexterityDone, int staminaDone)
{
	strength = strengthDone;
	dexterity = dexterityDone;
	stamina = staminaDone;
}

void Attributes::distribSocial(int charismaDone, int manipulationDone, int appearanceDone) // WARNING add parameter (string clanName)
{
	 charisma = charismaDone;
	 manipulation = manipulationDone;
	 appearance = appearanceDone;
}

void Attributes::distribMental(int perceptionDone, int intelligenceDone, int witsDone)
{
	 perception = perceptionDone;
	 intelligence = intelligenceDone;
	 wits = witsDone;
}

// For retrieve scores
vector<int> Attributes::getScoresPhysical()
{
	scoresPhysical.clear();
	scoresPhysical.push_back(strength);
	scoresPhysical.push_back(dexterity);
	scoresPhysical.push_back(stamina);
	
	return scoresPhysical;
}

vector<int> Attributes::getScoresSocial()
{
	scoresSocial.clear();
	scoresSocial.push_back(charisma);
	scoresSocial.push_back(manipulation);
	scoresSocial.push_back(appearance);
	
	return scoresSocial;
}

vector<int> Attributes::getScoresMental()
{
	scoresMental.clear();
	scoresMental.push_back(perception);
	scoresMental.push_back(intelligence);
	scoresMental.push_back(wits);
	
	return scoresMental;
}





/********************************************************************************************************/
string Clan::GetClanName()
{
	return clanName;
}

void Clan::SetClanName(string nomDuClan)
{
	clanName = nomDuClan;
}

string Clan::GetNickname()
{
	return nickname;
}

void Clan::SetNickname(string name)
{
	
}

string Clan::GetBackground()
{
	return background;
}

void Clan::SetBackground(string history)
{
	
}

string Clan::GetSect()
{
	return sect;
}

void Clan::SetSect(string secte)
{
	
}

string Clan::GetHaven()
{
	return haven;
}

void Clan::SetHaven(string maison)
{
	
}

string Clan::GetAppearance()
{
	return appearance;
}

void Clan::SetAppearance(string look)
{
	
}

string Clan::GetCharacter()
{
	return character;
}

void Clan::SetCharacter(string style)
{
	
}

void Clan::setDisciplines(int index, int value)
{
	valueDisciples[index] = value;
}

int Clan::GetValueDisciplines(int index)
{
	return valueDisciples[index];
}


string Clan::GetWeakness()
{
	return weakness;
}

void Clan::SetWeakness(string faiblesse)
{
	
}

string Clan::GetOrganization()
{
	return organization;
}

void Clan::SetOrganization(string organisation)
{
	
}

string Clan::GetNature_and_Demeanor()
{
	return nature_and_demeanor;
}

void Clan::SetNature_and_Demeanor(string nature)
{
	nature_and_demeanor = nature;
}





/********************************************************************************************************/
void Personnage::setNamePlayer(string nameOfPlayer)
{
	namePlayer = nameOfPlayer;
}
string Personnage::getNamePlayer()
{
	return namePlayer;
}
