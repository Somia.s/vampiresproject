#include "vampiresProject.h"
#include "vampires.cpp"
#include "firstInterface.cpp"
#include "secondInterface.cpp"
#include "parseFile.cpp"

int main(int argc, char *argv[])
{

	auto app = Gtk::Application::create(argc, argv);
	//Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv);

	FirstInterface firstInterface; // Creation of a vampire <=> open first interface only once

	//Shows the window and returns when it is closed.
	return app->run(firstInterface);
}
