#include "vampiresProject.h"
#include <set>


/********************************************************************************************************/
vector<string> File::tokenize(const string& str, string delim)
/** This function parse file containing 
 * @return NOTHING */
{ 
    vector<string> out;
    
    size_t start;
    size_t end = 0;
 
    while ((start = str.find_first_not_of(delim, end)) != std::string::npos)
    {
        end = str.find(delim, start);
        out.push_back(str.substr(start, end - start));
    }
    return out;
}



/********************************************************************************************************/
void File::readFileNatures()
/** This function parses file containing nature informations
 * @return NOTHING */
{
	ifstream fileN("natures.txt");

	if(fileN) // if file is opening
	{
	string line;
	string lineN;
	string naturePreviously;
	
		while(getline(fileN, line)) // while we have live to read
		{
			/// RETRIEVE NATURES
			size_t found = line.find("--");
			if (found!=std::string::npos)
			{
				string lineN = line.erase(0,2); // delete "--" (arguments = first position, how character need to remove)
				allNatures.insert(lineN); // insert into set
			}
			
			/// RETRIEVE DESCRIPTION OF NATURE
			size_t foundDescription = line.find("*");
			if (foundDescription!=std::string::npos)
			{
				string lineDescription = line.erase(0,2); // delete "*"	
				for (auto it = allNatures.begin(); it != allNatures.end(); ++it)
				{
					naturePreviously = *it; // retrieve last value = clan of line previously
				}
        		descriptionsNatures.insert(pair<string, string>(naturePreviously, lineDescription)); // insert into set
			}
		}
	}
	else
	{
		cout << "ERREUR: Impossible d'ouvrir le fichier Natures en lecture." << endl;
	}
}



/********************************************************************************************************/
void File::readFileClans()
/** This function parses file containing clan informations
 * @return NOTHING */
{	
	ifstream file("clans.txt");

	if(file) // if file is opening
	{
	string line;
	string lineC;

		while(getline(file, line)) //Tant qu'on n'est pas à la fin, on lit// while we have live to read
		{
		
			/// RETRIEVE NAME OF CLAN
			size_t found = line.find("--");
			if (found!=std::string::npos)
			{
				lineC = line.erase(0,2); // delete "--" (arguments = first position, how character need to remove)
				allClans.insert(lineC); // insert into set
			}
			
			/// RETRIEVE NICKNAME
			size_t foundN = line.find("Nickname:"); // line following name of clan
			if (foundN!=std::string::npos)
			{
				string lineN = line.erase(0,10); // delete "\tNickname:"
				allNicknames.insert(pair<string, string>(lineC,lineN)); // insert into map, lineC = name of clan just previously
			}		
				
			/// RETRIEVE DESCRIPTION OF CLAN
			size_t foundDescr = line.find("*");
			if (foundDescr!=std::string::npos)
			{
				string lineDescription = line.erase(0,2); // delete "*"				
				descriptionsClans.insert(pair<string, string>(lineC, lineDescription)); // insert into set
			}
			
			/// RETRIEVE DISCIPLINES
			size_t foundD = line.find("Clan Disciplines:");
			if (foundD!=std::string::npos)
			{
				string lineD = line.erase(0,18); // delete "\tClan Disciplines:"
				vector<string> tempMap; // initialization
				
				string delim = ",";
				tempMap = tokenize(lineD, delim);				
				allDisciplines.insert(pair<string, vector<string>>(lineC, tempMap)); // set into final map
			}	
		}
	}
	else
	{
		cout << "ERREUR: Impossible d'ouvrir le fichier Clans en lecture." << endl;
	}
}





/********************************************************************************************************/
set<string> File::getNatures()
{
	return allNatures;
}


/********************************************************************************************************/
string File::getDescriptionNature(string nature)
{	
    return descriptionsNatures[nature];
}



/********************************************************************************************************/
set<string> File::getClans()
{
	return allClans;
}



/********************************************************************************************************/
string File::getDescriptionClan(string clan)
{
    return descriptionsClans[clan];
}



/********************************************************************************************************/
string File::getNickname(string clan)
{
	return allNicknames[clan];
}


/********************************************************************************************************/
vector<string> File::getDiscipline(string clan)
/** This function return disciplines accordiant a clan
 * @param clan : name of clan chosen by player
 * @return several strings */
{
	vector<string> valuesDisciplines = allDisciplines[clan];
	return valuesDisciplines;
}
